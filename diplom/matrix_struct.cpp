#pragma once
#include <iostream>
#include <stdlib.h>

#include <math.h>
#include "matrix_struct.h"


int Init_MyDiagMatr2DStruct(
	int nx,				// Number of nodes along the axis X
	int ny,				// Number of nodes along the axis Y
	int nd,				// Number of diagonals
	MyDiagMatr2DStruct* DMG // Structure for store for the matrix in diagonal format
	)
{
	DMG->nx = nx;
	DMG->ny = ny; 
	DMG->nd = nd;

	DMG->a = (double*)malloc((nd * nx * ny + (3*nx -2)) * sizeof(double));

	return 0;
}
int MyConstr_Matr_2D5DS_RD(MyDiagMatr2DStruct* DMG) {
	int nx = DMG->nx;
	int ny = DMG->ny;
	int nd = DMG->nd;
	double* aa = DMG->a;
	int i = 0;
	for (i; i < 3*nx -2 ; i++) {
		aa[i] = 0.0;
	}
	int k = 1;
	for (i; i < nd * nx * ny + 3*nx -2; ) {
		aa[i] = 4.0;
		i++;
		//if ((i - (nd * nx - 4)) % nd * nx == 0) {
		if (k % nx == 0) {
			aa[i] = 0.0;
			k = 0;
		}
		else {
			aa[i] = -1.0;
		}
		i++;
		aa[i] = -1.0;
		i++;
		k++;
	}

	return 0;
}
MyVectorStruct fromReductionuToFullu(MyVectorStruct* U, MyVectorStruct* f, MyDiagMatr2DStruct* DMG) {
	MyVectorStruct vect;
	Init_MyVectorStruct(DMG->nx, DMG->ny, &vect);
	int i = 0;
	int k = 0;
	int t = 0;
	int h = 0;
	int nx = vect.nx;
	int ny = vect.ny;
	int shift = 3 * DMG->nx - 2;
	double* u = vect.u;
	double* matr = DMG->a;
	while (i < nx) {
		u[i] = 0.0;
		i++;
	}

	while (i < 2 * nx) {
		
		if (i % nx == 0) {
			u[i] = (f->u[i] 
				- matr[k + shift + 1] * U->u[U->nx] 
				- matr[k + shift + 2] * U->u[U->nx + U->nx]) 
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t];
			i += 2;
			k += 6;
			t++;
			continue;
		}
		if ((i + 1) % nx == 0) {
			//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
			u[i] = (f->u[i] 
				- matr[k + shift - 2] * U->u[U->nx + t - 1] 
				- matr[k + shift + 2] * U->u[U->nx + U->nx + t]) 
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t];
			i += 2;
			k += 6;
			t++;
			continue;
		}
		else {
			u[i] = (f->u[i]
				- matr[k + shift + 1] * U->u[U->nx + t - 1] // + ai0*u(i-1)0 
				- matr[k + shift + 1] * U->u[U->nx + t]		// + ci0*u(i+1)0
				- matr[k + shift + 2] * U->u[U->nx + U->nx + t])// + di0*ui2
					/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t];
			i += 2;
			k += 6;
			t++;
			continue;
		}
	}

	while (i < nx * ny) {
		//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
		if (i % nx == 0 ) {
			//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
			u[i] = (f->u[i]
				- matr[k + shift - 1] * U->u[U->nx - U->nx + t - 1] // 
				- matr[k + shift + 1] * U->u[U->nx + t]		// + ci0*u(i+1)0
				- matr[k + shift + 2] * U->u[U->nx + U->nx + t])// + di0*ui2
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t ];
			i += 2;
			k += 6;
			t++;
			continue;
		}
		if ((i + 1) % nx == 0) {
			//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
			u[i] = (f->u[i]
				- matr[k + shift - 2] * U->u[U->nx  + t - 1] // 
				- matr[k + shift - 1] * U->u[U->nx - U->nx + t - 1]		// + ci0*u(i+1)0
				- matr[k + shift + 2] * U->u[U->nx + U->nx + t])// + di0*ui2
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t ];
			i += 2;
			k += 6;
			t++;
			continue;
		}
		else {
			u[i] = (f->u[i]
				- matr[k + shift + 1] * U->u[U->nx + t - 1]
				- matr[k + shift + 2] * U->u[U->nx - U->nx + t - 1]
				- matr[k + shift + 1] * U->u[U->nx + t]
				- matr[k + shift + 2] * U->u[U->nx + U->nx + t])
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t];
			i += 2;
			k += 6;
			t++;
			continue;
		}

	}			

	while (i < nx * ny + nx) {
		//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
		if (i % nx == 0) {
			//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
			u[i] = (f->u[i]
				- matr[k + shift - 1] * U->u[U->nx - U->nx + t - 1]
				- matr[k + shift + 1] * U->u[U->nx + t])
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t];
			i += 2;
			k += 6;
			t++;
			continue;
		}
		if ((i + 1) % nx == 0) {
			//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
			u[i] = (f->u[i]
				- matr[k + shift - 2] * U->u[U->nx + t - 1]
				- matr[k + shift - 1] * U->u[U->nx - U->nx + t - 1])
				/ (matr[k + shift]);
			u[i + 1] = 0;
			i += 2;
			k += 6;
			t++;
			continue;
		}
		else {
			//std::cout << "i = " << i << " k = " << k << " t = " << t << std::endl;
			u[i] = (f->u[i]
				- matr[k + shift - 2] * U->u[U->nx + t - 1] // 
				- matr[k + shift - 1] * U->u[U->nx - U->nx + t - 1]		// + ci0*u(i+1)0
				- matr[k + shift + 1] * U->u[U->nx + t])// + di0*ui2
				/ (matr[k + shift]);
			u[i + 1] = U->u[U->nx + t];
			i += 2;
			k += 6;
			t++;
			continue;
		}
	}

	while (i < nx * ny + 2 * nx) {
		u[i] = 0;
		i++;
	}

	return vect;
}
MyVectorStruct fromFulluToReductionu(MyVectorStruct* U) {
	MyVectorStruct vect;
	int nx = U->nx;
	int ny = U->ny;
	int k = 0;
	Init_MyVectorStruct((nx + 1) / 2 - 1, nx + 1, &vect);
	for (int i = nx + 1; i < nx * ny + nx;) {
		vect.u[(nx + 1) / 2 - 1 + k] = U->u[i];
		i += 2;
		k++;
	}
	for (int i = 0; i < vect.nx; i++) {
		vect.u[i] = 0.0;
	}
	for (int i = vect.nx*vect.ny + vect.nx; i < vect.nx * vect.ny + 2*vect.nx; i++) {
		vect.u[i] = 0.0;
	}
	return vect;
}
int MyConstr_MatrReduction(MyDiagMatr2DStruct* DMG) {
	int nx = DMG->nx;
	int ny = DMG->ny;
	int nd = DMG->nd;
	double* aa = DMG->a;
	for (int i = 0; i < 3 * nx - 2; i++) {
		aa[i] = 0.0;
	}
	int i = 0;
	int coun = nx;
	while (i < nd * nx) {
		if (i % nd * nx == 0 && i < nd) {
			aa[i + 3 * nx - 2] = 3.5;
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun > 1 ) {
			aa[i + 3 * nx - 2] = 3.25;
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			aa[i + 3 * nx - 2] = 3.5;
			aa[i + 3 * nx - 1] = 0;
			aa[i + 3 * nx - 0] = -0.25;
			i++;
			i++;
			i++;
			coun=nx;
			continue;
		}
		aa[i + 3 * nx - 2] = -0.25;
		i++;
	}

	while (i < nd * nx * (ny - 1)) {
		if (i % nd * nx == 0 && coun == nx) {
			aa[i + 3 * nx - 2] = 3.25;
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			aa[i + 3 * nx - 2] = 3.0;
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			aa[i + 3 * nx - 2] = 3.25;
			aa[i + 3 * nx - 1] = 0;
			aa[i + 3 * nx - 0] = -0.25;
			i++;
			i++;
			i++;
			coun = nx;
			continue;
		}
		aa[i + 3 * nx - 2] = -0.25;
		i++;
	}
	while (i < nd * nx * ny) {
		if (i % nd * nx == 0 && coun == nx) {
			aa[i + 3 * nx - 2] = 3.5;
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun >1) {
			aa[i + 3 * nx - 2] = 3.25;
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun  == 1) {
			aa[i + 3 * nx - 2] = 3.5;
			aa[i + 3 * nx - 1] = 0;
			aa[i + 3 * nx - 0] = -0.25;
			i++;
			i++;
			i++;
			coun = nx;
			continue;
		}
		aa[i + 3 * nx - 2] = -0.25;
		i++;
	}

	return 0;
}

int Init_justVector(int n, justVector* v) {
	v->n = n;
	v->u = (double*)malloc((n) * sizeof(double));
	return 0;
}
int ZeroJustVector(justVector* just) {
	for (int i = 0; i < just->n; i++) {
		just->u[i] = 0;
	}
	return 0;
}
int Init_MyVectorStruct(int nx,int ny, MyVectorStruct* U) {
	U->nx = nx;
	U->ny = ny;
	U->u = (double*)malloc((nx*ny+2*nx) * sizeof(double));

	return 0;
}
int Init_My123Vector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx; i++) {
		u[i] = 0;
	}
	for (i; i < nx * ny + nx; i++) {
		u[i] = i - nx + 1;
	}
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0;
	}
	return 0;
}
int Init_MyEyeVector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx; i++) {
		u[i] = 0.0;
	}
	for (i; i < nx * ny + nx; i++) {
		u[i] = 1.0;
	}
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0.0;
	}
	return 0;
}

int Init_sinxsiny(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx; i++) {
		u[i] = 0.0;
	}
	for (i; i < nx * ny + nx; i++) {
		u[i] = sin(i-nx)*sin(i-nx);
	}
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0.0;
	}
	return 0;
}


int Init_MyEye7Vector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx; i++) {
		u[i] = 0.0;
	}
	int j = 0;
	while (j < nx) {
		while (i < nx * (j + 1) + nx) {
			if (j % 2 == 0) {
				u[i] = 2.0;
				i++;
			}
			else {
				u[i] = 10.0;
				i++;
			}
		}
		j++;
	}
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0.0;
	}
	return 0;
}

int ReductionMatrixAndVectorF(MyDiagMatr2DStruct* full_m, MyDiagMatr2DStruct* reduction_m, MyVectorStruct* full_f, MyVectorStruct* reduction_f, justVector* reduction_u) {
	reduction_m->nd = full_m->nd;
	reduction_m->nx = (full_m->nx + 1) / 2;
	reduction_m->ny = (full_m->ny + 1) / 2;
	reduction_m->a = (double*)malloc((reduction_m->nd * reduction_m->nx * reduction_m->ny + (3 * reduction_m->nx - 2)) * sizeof(double));
	reduction_f->nx = reduction_m->nx;
	reduction_f->ny = reduction_m->ny;
	reduction_f->u = (double*)malloc((reduction_f->nx * reduction_f->ny + 2 * reduction_f->nx) * sizeof(double));
	double* red_u = reduction_u->u;
	int nx = reduction_m->nx;
	int ny = reduction_m->ny;
	int nd = reduction_m->nd;
	int fnx = full_m->nx;
	int fny = full_m->ny;
	double* aa = reduction_m->a;
	double* A = full_m->a;
	int i = 0;
	int ir = sqrt(reduction_u->n);
	int shift_full = 3 * full_m->nx - 2;
	int shift_full_f = full_m->nx;
	int shift_reduction = 3 * reduction_m->nx - 2;
	int shift_reduction_f = reduction_m->nx;
	int i_v = 0;
	while (i_v < nx) {
		reduction_f->u[i_v] = 0;
		i_v++;
	}
	while (i < 3 * reduction_m->nx - 2) {
		aa[i] = 0;
		i++;
	}
	int t = 0;
	i = 0;
	int k = 0;
	int j = shift_full_f;
	int coun = nx;
	while (i < nd * nx) {
		if (i == 0) {
			// k = 0
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k] 
									- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c00a10/e10
									- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d00b01/e01

			aa[shift_reduction + i + 1] = -1 * A[shift_full + k + 1] * A[shift_full + k + 4] / A[shift_full + k + 3]; //c00c10/e10

			aa[shift_reduction + i + 2] = -1 * A[shift_full + k + 2] * A[shift_full + k + fnx * nd + 2] / A[shift_full + k + fnx * nd]; //d00d01/e01

			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k + 1] * (A[shift_full + k + 5] * red_u[t] - full_f->u[j+1]) / A[shift_full + k + 3] //c00(d10u11 + f10)/e10
				+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd + 1] * red_u[t] - full_f->u[j + fnx]) /A[shift_full + k + fnx * nd]; // d00(c01u11+f201)/e01
				
			j += 2;
			i+=3;
			i_v++;
			coun--;
			k += 6;
			t++;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			// k = 6
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k]
				- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3]			// a20c10/e10
				- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3]			// c20a30/e30
				- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd];	// d20b21/e21

			aa[shift_reduction + i + 1] = -1 * A[shift_full + k + 1] * A[shift_full + k + 4] / A[shift_full + k + 3]; // c20c30/e30

			aa[shift_reduction + i + 2] = -1 * A[shift_full + k + 2] * A[shift_full + k + fnx * nd + 2] / A[shift_full + k + fnx * nd]; // d20d21/e21
			//std::cout << "k = " << k << " j = " << j << " t = " << t << " i_v = "<<i_v<<std::endl;
			//std::cout << "A[shift_full + k + 2] = " << A[shift_full + k + 2] << " (A[shift_full + k + fnx * nd + 1] * red_u[t] - full_f->u[j + fnx]) = " << (A[shift_full + k + fnx * nd + 1] * red_u[t] - full_f->u[j + fnx]) << " full_f->u[j + fnx] = " << full_f->u[j + fnx] << " A[shift_full + k + nx * nd] = " << A[shift_full + k + fnx * nd] << std::endl;
			//std::cout << " j = " << j << " full_f->u[j + fnx] " << full_f->u[j + fnx] <<  std::endl;
			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 2] * (A[shift_full + k - 1] * red_u[t - 1] - full_f->u[j - 1]) / A[shift_full + k - 3] //a20(d10u11 + f10)/e10
				+ A[shift_full + k + 1] * (A[shift_full + k + 5] * red_u[t] - full_f->u[j + 1]) / A[shift_full + k + 3] //c20(d30u31 + f30)/e30
				+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t - 1] + A[shift_full + k + fnx * nd + 1] * red_u[t] - full_f->u[j + fnx])  / A[shift_full + k + fnx * nd]; //d20(a21u11 + c21u31 + f21)/e21
			j += 2;
			i += 3;
			i_v++;
			coun--;
			k += 6;
			t++;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			t--;
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k] 
									- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3] //a60c50/e50
									- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d60b61/e61
			
			aa[shift_reduction + i + 1] = 0; // ��� ������
			aa[shift_reduction + i + 2] = -1 * A[shift_full + k + 2] * A[shift_full + k + fnx * nd + 2] / A[shift_full + k + fnx * nd]; //d60d61/e61

			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 2] * (A[shift_full + k - 1] * red_u[t] - full_f->u[j - 1]) / A[shift_full + k - 3] //a60(d50u51 + f50)/e50
				+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t] - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd]; // d60(a61u51+f61)/e61
			j += fnx + 1;
			i += 3;
			i_v++;
			coun = nx;
			k += nd*fnx +3;
			t++;
			continue;
		}
	}

	while (i < nd * nx * (ny - 1)) {
		if (i % nd * nx == 0 && coun == nx) {
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k]
				- A[shift_full + k - 1] * A[shift_full + k - fnx * nd + 2] / A[shift_full + k - fnx * nd] //b02d01/e01
				- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c02a12/e12
				- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d02b03/e03
			
			aa[shift_reduction + i + 1] = -A[shift_full + k + 1] * A[shift_full + k + 4] / A[shift_full + k + 3]; //c02c12/e12

			aa[shift_reduction + i + 2] = -A[shift_full + k + 2] * A[shift_full + k + fnx * nd + 2] / A[shift_full + k + fnx * nd]; //d02d03/e03

			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 1] * (A[shift_full + k - fnx * nd + 1] * red_u[t - ir] - full_f->u[j - fnx]) / A[shift_full + k - fnx * nd] //b02(c01u11 + f01)/e01
				+ A[shift_full + k + 1] * (A[shift_full + k + 2] * red_u[t - ir] + A[shift_full + k + 5] * red_u[t] - full_f->u[j + 1]) / A[shift_full + k + 3] // c02(b12u11 + d12u13 + f12)/e12
				+ A[shift_full + k + 2] * (A[shift_full + k + 4] * red_u[t] - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd];// d02(c03u13 + f03)/e03

				j += 2;
				i += 3;
				i_v++;
				coun--;
				k += 6;
				t++;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k]
				- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3] //a22c12/e12
				- A[shift_full + k - 1] * A[shift_full + k - fnx * nd + 2] / A[shift_full + k - fnx * nd] //b22d21/e21
				- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c22a32/e32
				- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d22b23/e23
			
			aa[shift_reduction + i + 1] = -A[shift_full + k + 1] * A[shift_full + k + 4] / A[shift_full + k + 3]; //c22c32/e32

			aa[shift_reduction + i + 2] = -A[shift_full + k + 2] * A[shift_full + k + fnx * nd + 2] / A[shift_full + k + fnx * nd]; //d22d23/e23
			
			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 2] * (A[shift_full + k - 4]            * red_u[t - ir - 1] + A[shift_full + k - 1]            * red_u[t - 1]  - full_f->u[j - 1])   / A[shift_full + k - 3]         // a22(b12u11 + d12u13 + f12)/e12
				+ A[shift_full + k - 1] * (A[shift_full + k - fnx * nd - 2] * red_u[t - ir - 1] + A[shift_full + k - fnx * nd + 1] * red_u[t - ir] - full_f->u[j - fnx]) / A[shift_full + k - fnx * nd]  // b02(a21u11 + c21u31 + f21)/e21
				+ A[shift_full + k + 1] * (A[shift_full + k + 2]            * red_u[t - ir]     + A[shift_full + k + 5]            * red_u[t]      - full_f->u[j + 1])   / A[shift_full + k + 3]         // c22(b32u31 + d32u33 + f32)/e32
				+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t - 1]      + A[shift_full + k + fnx * nd + 1] * red_u[t]      - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd]; // d02(a23u13 + c23u33 + f23)/e23
			j += 2;
			i += 3;
			i_v++;
			coun--;
			k += 6;
			t++;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			t--;
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k]
				- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3] //a62c52/e52
				- A[shift_full + k - 1] * A[shift_full + k - fnx * nd + 2] / A[shift_full + k - fnx * nd] //b62d61/e61
				//- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c22a32/e32
				- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d62b63/e63

			aa[shift_reduction + i + 1] = 0; //

			aa[shift_reduction + i + 2] = -A[shift_full + k + 2] * A[shift_full + k + fnx * nd + 2] / A[shift_full + k + fnx * nd]; //d62d63/e63

			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 2] * (A[shift_full + k - 4] * red_u[t - ir] + A[shift_full + k - 1] * red_u[t] - full_f->u[j - 1]) / A[shift_full + k - 3]         // a22(b12u11 + d12u13 + f12)/e12
				+ A[shift_full + k - 1] * (A[shift_full + k - fnx * nd - 2] * red_u[t - ir] + 0*A[shift_full + k - fnx * nd + 1] * red_u[t - ir] - full_f->u[j - fnx]) / A[shift_full + k - fnx * nd]  // b02(a21u11 + c21u31 + f21)/e21
				//+ A[shift_full + k + 1] * (A[shift_full + k + 2] * red_u[t - ir] + A[shift_full + k + 5] * red_u[t] - full_f->u[j + 1]) / A[shift_full + k + 3]         // c22(b32u31 + d32u33 + f32)/e32
				+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t] + 0*A[shift_full + k + fnx * nd + 1] * red_u[t] - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd]; // d02(a23u13 + c23u33 + f23)/e23


			j += fnx + 1;
			i += 3;
			i_v++;
			coun = nx;
			k += nd * fnx + 3;
			t++;
			continue;
		}
	}

	while (i < nd * nx * ny) {
		if (i % nd * nx == 0 && coun == nx) {
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k]
				//- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3] //a22c12/e12
				- A[shift_full + k - 1] * A[shift_full + k - fnx * nd + 2] / A[shift_full + k - fnx * nd] //b06d05/e05
				- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c06a16/e16
				//- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d22b23/e23
				;
			aa[shift_reduction + i + 1] = -A[shift_full + k + 1] * A[shift_full + k + 4] / A[shift_full + k + 3]; //c06c16/e16

			aa[shift_reduction + i + 2] =  aa[shift_reduction + i - 1];

			reduction_f->u[i_v] = full_f->u[j]
				//+ A[shift_full + k - 2] * (A[shift_full + k - 1] * red_u[t - 3] + A[shift_full + k + 5] * red_u[t] - full_f->u[j - 1]) / A[shift_full + k - 3] // a22(b12u11 + d12u13 + f12)/e12
				+ A[shift_full + k - 1] * (A[shift_full + k - fnx * nd + 1] * red_u[t - ir] - full_f->u[j - fnx]) / A[shift_full + k - fnx * nd] //b02(a21u11 + c21u31 + f21)/e21
				+ A[shift_full + k + 1] * (A[shift_full + k + 2] * red_u[t - ir] - full_f->u[j + 1]) / A[shift_full + k + 3] // c22(b32u31 + d32u33 + f32)/e32
				//+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t] + A[shift_full + k + 4] * red_u[t + 1] - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd];// d02(a23u13 + c23u33 + f23)/e23
				;

			j += 2;
			i += 3;
			i_v++;
			coun--;
			k += 6;
			t++;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			aa[shift_reduction + i] = A[shift_full + k]
				- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3] //a22c12/e12
				- A[shift_full + k - 1] * A[shift_full + k - fnx * nd + 2] / A[shift_full + k - fnx * nd] //b06d05/e05
				- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c06a16/e16
				//- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d22b23/e23
				;
			aa[shift_reduction + i + 1] = -A[shift_full + k + 1] * A[shift_full + k + 4] / A[shift_full + k + 3]; //c06c16/e16

			aa[shift_reduction + i + 2] = aa[shift_reduction + i - 1];

			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 2] * (A[shift_full + k - 1] * red_u[t - ir - 1] - full_f->u[j - 1]) / A[shift_full + k - 3] // a22(b12u11 + d12u13 + f12)/e12
				+ A[shift_full + k - 1] * (A[shift_full + k - fnx * nd - 2] * red_u[t - ir - 1] + A[shift_full + k - fnx * nd + 1] * red_u[t - ir] - full_f->u[j - fnx]) / A[shift_full + k - fnx * nd] //b02(a21u11 + c21u31 + f21)/e21
				+ A[shift_full + k + 1] * (A[shift_full + k + 2] * red_u[t - ir] - full_f->u[j + 1]) / A[shift_full + k + 3] // c22(b32u31 + d32u33 + f32)/e32
				//+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t] + A[shift_full + k + 4] * red_u[t + 1] - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd];// d02(a23u13 + c23u33 + f23)/e23
			;

			j += 2;
			i += 3;
			i_v++;
			coun--;
			k += 6;
			t++;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			//std::cout << "i = " << i << " k = " << k << " j = " << j << " t = " << t << " i_v = " << i_v << std::endl;
			t--;
			aa[shift_reduction + i] = A[shift_full + k]
				- A[shift_full + k - 2] * A[shift_full + k - 2] / A[shift_full + k - 3] //a22c12/e12
				- A[shift_full + k - 1] * A[shift_full + k - fnx * nd + 2] / A[shift_full + k - fnx * nd] //b06d05/e05
				//- A[shift_full + k + 1] * A[shift_full + k + 1] / A[shift_full + k + 3] //c06a16/e16
				//- A[shift_full + k + 2] * A[shift_full + k + fnx * nd - 1] / A[shift_full + k + fnx * nd]; //d22b23/e23
				;
			aa[shift_reduction + i + 1] = 0; //c06c16/e16

			aa[shift_reduction + i + 2] = aa[shift_reduction + i - 1];

			reduction_f->u[i_v] = full_f->u[j]
				+ A[shift_full + k - 2] * (A[shift_full + k - 1] * red_u[t - ir] - full_f->u[j - 1]) / A[shift_full + k - 3] // a22(b12u11 + d12u13 + f12)/e12
				+ A[shift_full + k - 1] * (A[shift_full + k - fnx * nd - 2] * red_u[t - ir] - full_f->u[j - fnx]) / A[shift_full + k - fnx * nd] //b02(a21u11 + c21u31 + f21)/e21
				//+ A[shift_full + k + 1] * (A[shift_full + k + 2] * red_u[t - 2] + A[shift_full + k + 5] * red_u[t + 1] - full_f->u[j + 1]) / A[shift_full + k + 3] // c22(b32u31 + d32u33 + f32)/e32
				//+ A[shift_full + k + 2] * (A[shift_full + k + fnx * nd - 2] * red_u[t] + A[shift_full + k + 4] * red_u[t + 1] - full_f->u[j + fnx]) / A[shift_full + k + fnx * nd];// d02(a23u13 + c23u33 + f23)/e23
				;


			j += fnx + 1;
			i += 3;
			i_v++;
			coun = nx;
			k += nd * fnx + 3;
			t++;
			continue;
		}



	}
	while (i_v < nx * ny + 2 * nx) {
		reduction_f->u[i_v] = 0;
		i_v++;
	}

	return 0;
}
justVector from_Full_u_To_Reduction_u(MyVectorStruct* U) {
	justVector t;
	int nx = U->nx;
	int ny = U->ny;
	Init_justVector((nx-1)* (ny - 1)/4, &t);
	int j = 0;
	int i = nx;
	int k = 0;
	while (i < nx * ny + nx) {
		if (j % 2 == 0) {
			i += nx + 1;
			j++;
			continue;
		}
		else {
			while (i < nx * (j + 1) + nx) {
				t.u[k] = U->u[i];
				i += 2;
				k++;
			}
			j++;
		}
	}
	return t;
}

justVector from_Full_u_To_Reduction_u2(MyVectorStruct* U) {
	justVector t;
	int nx = U->nx;
	int ny = U->ny;
	Init_justVector((nx + 1) * (ny + 1) / 4, &t);
	int j = 0;
	int i = nx;
	int k = 0;
	while (i < nx * ny + nx) {
		
		if (j % 2 == 1) {
			//std::cout << "i = " << i << " j = " << j << " k = " << k << std::endl;
			i += nx - 1;
			j++;
			continue;
		}
		else {
			//std::cout << "else i = " << i << " j = " << j << " k = " << k << std::endl;
			while (i < nx * (j+2)) {
				t.u[k] = U->u[i];
				i += 2;
				k++;
			}
			j++;
		}
	}
	return t;
}

int Init_MyReductionVector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	while (i< nx) {
		u[i] = 0.0;
		i++;
	}
	while (i < 2 * nx) {
		if (i % nx == 0 || (i+1)%nx ==0) {
			u[i] = 3.0;
		}
		else {
			u[i] = 2.5;
		}
		i++;
	}
	while (i < nx*ny) {
		if (i % nx == 0 || (i + 1) % nx == 0) {
			u[i] = 2.5;
		}
		else {
			u[i] = 2.0;
		}
		i++;
	}
	while (i < nx*ny + nx) {
		if (i % nx == 0 || (i + 1) % nx == 0) {
			u[i] = 3.0;
		}
		else {
			u[i] = 2.5;
		}
		i++;
	}
	while (i < nx*ny+2*nx) {
		u[i] = 0;
		i++;
	}

	return 0;
}

int Init_MyRandomVector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx; i++) {
		u[i] = 0.0;
	}
	for (i; i < nx * ny + nx; i++) {
		u[i] = rand()%91;
	}
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0.0;
	}
	return 0;
}


int Init_MyTooEyeVector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx; i++) {
		u[i] = 0.0;
	}
	for (i; i < nx * ny + nx; i++) {
		u[i] = 2.0;
	}
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0.0;
	}
	return 0;
}

int Init_MyZeroVector(MyVectorStruct* U) {
	int i = 0;
	int nx = U->nx;
	int ny = U->ny;
	double* u = U->u;
	for (i; i < nx * ny + 2 * nx; i++) {
		u[i] = 0.0;
	}
	return 0;
}


int My_Myltiplication_MonV(MyDiagMatr2DStruct* DMG, MyVectorStruct* U, MyVectorStruct* res) {
	double* u_new = res->u;
	double* u_old = U->u;
	double* a = DMG->a;
	int nx = U->nx;
	int ny = U->ny;
	int k = nx;
	int shift = nx * 3 - 2;
	int i = shift;
	//for (int i = nx; i < nx*ny+2*nx;) {
	//for (int j = 0; j < nx * ny ;) {
	//	u_new[k] = a[i - nx]            * u_old[k-nx] 
	//			 + a[i - nx + shift - 2]* u_old[k - nx + 2] 
	//			 + a[i - nx + shift]    * u_old[k - nx + 3] 
	//			 + a[i - nx + shift + 1]* u_old[k - nx + 4] 
	//   			 + a[i - nx + shift + 2]* u_old[k - nx + shift - 2];

	//			//+ a[i - nx+shift+2] * u_old[k - nx + shift+nx-1];
	//	std::cout <<
	//		a[i - nx ]			  << "a[" << i-nx               << "] " << u_old[k - nx]			 << "u[" << k-nx <<               "] " <<
	//		a[i - nx + shift - 2] << "a[" << i - nx + shift - 2 << "] " << u_old[k - nx + 2]		 << "u[" << k - nx + 2 <<		  "] " <<
	//		a[i - nx + shift]	  << "a[" << i - nx + shift     << "] " << u_old[k - nx + 3]  	     << "u[" << k - nx + 3 <<		  "] " <<
	//		a[i - nx + shift + 1] << "a[" << i - nx + shift + 1 << "] " << u_old[k - nx + 4]	     << "u[" << k - nx + 4 <<		  "] " <<
	//		a[i - nx + shift + 2] << "a[" << i - nx + shift + 2 << "] " << u_old[k - nx + shift - 2] << "u[" << k - nx + shift - 2 << "] " << 
	//		"  k = " << k << 
	//		"  i = " << i << std::endl;
	//		//a[i - nx + shift + 2] << " " << u_old[k - nx + shift + nx] << "  i = "<<i<< std::endl;
	//	k++;
	//	j++;
	//	i += 3;
	//}
	for (int j = 0; j < nx * ny;) {
		u_new[k] = 0;
		u_new[k] = a[i - shift]		* u_old[k - nx]
				 + a[i - 2]			* u_old[k - 1]
				 + a[i]				* u_old[k]
				 + a[i + 1]			* u_old[k + 1]
				 + a[i + 2]			* u_old[k + nx];

		//std::cout <<
		//	a[i - shift] << "a[" << i - shift << "] "	  << u_old[k - nx]	  << "u[" << k - nx    << "] " <<
		//	a[i - 2]	 << "a[" << i - 2	  << "] "	  << u_old[k - 1]     << "u[" << k - 1     << "] " <<
		//	a[i]		 << "a[" << i		  << "] "	  << u_old[k]         << "u[" << k		   << "] " <<
		//	a[i + 1]	 << "a[" << i + 1	  << "] "	  << u_old[k + 1]	  << "u[" << k + 1	   << "] " <<
		//	a[i + 2]	 << "a[" << i + 2	  << "] "	  << u_old[k + nx]    << "u[" << k + nx    << "] " <<
		//	"  shift = " << shift <<
		//	"  k = " << k <<
		//	"  i = " << i << std::endl;
		k++;
		j++;
		i += 3;
	}

	return 0;
}


int VectorSub(MyVectorStruct* v1, MyVectorStruct* v2, MyVectorStruct* res) {
	double* vv1 = v1->u;
	double* vv2 = v2->u;
	double* ress = res->u;
	int nx = v1->nx;
	int ny = v1->ny;
	for (int i = nx; i < nx * ny + nx; i++) {
		ress[i] = 0;
		ress[i] = vv1[i] - vv2[i];
	}
	return 0;
}

int VectorEqual(MyVectorStruct* v1, MyVectorStruct* v2) {
	double* vv1 = v1->u;
	double* vv2 = v2->u;
	int nx = v1->nx;
	int ny = v1->ny;
	for (int i = nx; i < nx * ny + nx; i++) {
		vv1[i] = vv2[i];
	}
	return 0;
}

int VectorPlus(MyVectorStruct* v1, MyVectorStruct* v2, MyVectorStruct* res) {
	double* vv1 = v1->u;
	double* vv2 = v2->u;
	double* ress = res->u;
	int nx = v1->nx;
	int ny = v1->ny;
	for (int i = nx; i < nx * ny + nx; i++) {
		ress[i] = 0;
		ress[i] = vv1[i] + vv2[i];
	}
	return 0;
}


double VectorScalar(MyVectorStruct* v1, MyVectorStruct* v2) {
	double* vv1 = v1->u;
	double* vv2 = v2->u;
	int nx = v1->nx;
	int ny = v1->ny;
	double res = 0;
	for (int i = nx; i < nx * ny + nx; i++) {
		res += vv1[i] * vv2[i];
	}
	return sqrt(res);
}


double VectorError(MyVectorStruct* v1, MyVectorStruct* v2) {
	double* vv1 = v1->u;
	double* vv2 = v2->u;
	int nx = v1->nx;
	int ny = v1->ny;
	double res = 0;
	double t = 0;
	for (int i = nx; i < nx * ny + nx; i++) {
		t = abs(abs(vv1[i]) - abs(vv2[i]));
		res += t*t;
	}
	return sqrt(res);
}


int VectorMyltiplicationOnScalar(MyVectorStruct* v1, double scalar) {
	double* vv1 = v1->u;
	int nx = v1->nx;
	int ny = v1->ny;
	for (int i = nx; i < nx * ny + nx; i++) {
		vv1[i]= scalar * vv1[i];
	}
	return 0;
}


MyVectorStruct Prolongation(MyVectorStruct* v, MyDiagMatr2DStruct* matr, MyVectorStruct* f) {
	MyVectorStruct res;
	Init_MyVectorStruct(v->nx, v->ny, &res);
	int i = 0;
	int nx = res.nx;
	int ny = res.ny;
	int t = 0;
	int j = 0;
	int mi = 3 * matr->nx - 2;
	int iv = 0;
	int k = 0;
	double* A = matr->a;
	while (i < nx) {
		res.u[i] = 0;
		i++;
	}

	while (j < nx) {
		
		if (j == 0) {
			while (i < nx * (j + 2)) {
				if (i % 2 == 1) {
					res.u[i] = v->u[i];
					
					i++;
					continue;
				}
				else {
					res.u[i] = (-matr->a[mi + 1] * v->u[i - 1]
						//+ matr->a[mi - 1] * v->u[i - nx]
						- matr->a[mi + 4] * v->u[i + 1]
						- matr->a[mi + 5] * v->u[i + nx]
						+ f->u[i]) / matr->a[mi + 3];
					i++; 
					mi += 6;
					continue;
				}
			}
			j++;
			continue;
		}
		if (j < nx - 1) {
			if (j % 2 == 0) {
				while (i < nx * (j + 2)) {
					if (i % nx == 1) {
						res.u[i] = (-matr->a[mi + 1] * v->u[i - 1]
							- matr->a[mi - 1] * v->u[i - nx]
							- matr->a[mi + 4] * v->u[i + 1]
							- matr->a[mi + 5] * v->u[i + nx]
							+ f->u[i]) / matr->a[mi + 3];
						i++;
						mi += 6;
						continue;
					}
					if (i % nx - 1 != 0) {
						if (i % 2 == 1) {
							res.u[i] = v->u[i];
							i++;
							continue;
						}
						else {
							res.u[i] = (-matr->a[mi + 1] * v->u[i - 1]
								- matr->a[mi - 1] * v->u[i - nx]
								- matr->a[mi + 4] * v->u[i + 1]
								- matr->a[mi + 5] * v->u[i + nx]
								+ f->u[i]) / matr->a[mi + 3];
							i++;
							mi += 6;
							continue;
						}
					}
					if (i % nx - 1 == 0) {
						res.u[i] = ( -matr->a[mi + 1] * v->u[i - 1]
							- matr->a[mi - 1] * v->u[i - nx]
							//+ matr->a[mi + 4] * v->u[i + 1]
							- matr->a[mi + 5] * v->u[i + nx]
							+ f->u[i]) / matr->a[mi + 3];
						i++;
						mi += 6;
						continue;
					}
				}
			}
			else {
				while (i < nx * (j + 2)) {
					if (i % 2 == 1) {
						res.u[i] = v->u[i];
						i++;
						continue;
					}
					else {
						res.u[i] = (-matr->a[mi + 1] * v->u[i - 1]
							- matr->a[mi - 1] * v->u[i - nx]
							- matr->a[mi + 4] * v->u[i + 1]
							- matr->a[mi + 5] * v->u[i + nx]
							+ f->u[i]) / matr->a[mi + 3];
						i++;
						mi += 6;
						continue;
					}
				}
			}
			j++;
			continue;
		}
		if (j % (nx -1) == 0) {
			//std::cout << "i = " << i << " j = " << j << " t = " << t << std::endl;
			while (i < nx * (j + 2)) {
				if (i % 2 == 1) {
					res.u[i] = v->u[i];
					i++;
					continue;
				}
				else {
					res.u[i] = (-matr->a[mi + 1] * v->u[i - 1]
						- matr->a[mi - 1] * v->u[i - nx]
						- matr->a[mi + 4] * v->u[i + 1]
						//+ matr->a[mi + 5] * v->u[i + nx]
						+ f->u[i]) / matr->a[mi + 3];
					i++;
					mi += 6;
					continue;
				}
			}
			j++;
			continue;
		}
	}


	while (i < nx * ny + 2 * nx) {
		res.u[i] = 0;
		i++;
	}


	return res;

}

MyVectorStruct ProlongationVector(MyVectorStruct* v, justVector* redus) {
	MyVectorStruct res;
	Init_MyVectorStruct(v->nx * 2 - 1, v->ny * 2 - 1, &res);
	int i = 0;
	int j = 0;
	int k = 0;
	int nx = res.nx;
	int ny = res.ny;
	int vi = v-> nx;
	while (i < nx) {
		res.u[i] = 0;
		i++;
	}
	int t = 0;
	while (i < nx * ny + nx) {
		if (j % 2 == 0) {
			while (i < nx * (j + 1) + nx) {
				//std::cout <<"i = " <<i <<" j = "<<j<< " t = " << t << std::endl;
				res.u[i] = v->u[vi + t];
				res.u[i + 1] = 0;
				t++;
				i+=2;
			}
			j++;
			continue;
		}
		else {
			while (i < nx * (j + 1) + nx) {
				res.u[i] = redus->u[k];
				res.u[i + 1] = 0;
				i += 2;
				k++;
			}
			j++;
		}
	}
	while (i < nx*ny+2*nx) {
		res.u[i] = 0;
		i++;
	}


	return res;
}

justVector A31u1(MyVectorStruct* u) {
	justVector res;
	int nx = u->nx;
	Init_justVector((nx*nx-1)/2,&res);
	int j = 0;
	int k = 0;
	int i = 0;
	int t = 0;
	int count = (nx - 1) / 2;
	while (j < nx) {
		
		if (j % 2 == 0) {
			//std::cout << "j = " << j << " i = " << i << " k = " << k << " t = " << t << std::endl;
			while (k < count) {
				res.u[k] = -1*(u->u[i + nx - (nx - 1) * t / 2] + u->u[i + nx - (nx - 1) * t / 2 + 1]);
				i++;
				k++;
				}
			j++;
			t++;
			count+= (nx + 1) / 2;
		}
		else {
			//std::cout << "j = " << j << " i = " << i << " k = " << k << " t = " << t   <<std::endl;
			while (k < count) {
				res.u[k] = -1*(u->u[i + nx - (nx - 1) * t / 2] + u->u[i + nx - (nx - 1) * (t-1) / 2 + 1]);
				i++;
				k++;
			}
			j++;
			count += (nx - 1) / 2;
			//t++;
		}
	}
	return res;
}

// ��������
int A31u1just(justVector* u1, MyDiagMatr2DStruct *matr, justVector* A31u1) {
	int nx = matr->nx;
	int j = 0;
	int k = 0;
	int i = 0;
	int t = 0;
	int count = (nx - 1) / 2;
	while (j < nx) {

		if (j % 2 == 0) {
			//std::cout << "j = " << j << " i = " << i << " k = " << k << " t = " << t << std::endl;
			while (k < count) {
				//A31u1->u[k] = -1 * (u1->u[i + nx - (nx - 1) * t / 2] + u1->u[i + nx - (nx - 1) * t / 2 + 1]);
				A31u1->u[k] = -1 * u1->u[i - (nx - 1) * t / 2] + -1*u1->u[i + 1 - (nx - 1) * t / 2];
				i++;
				k++;
			}
			j++;
			t++;
			count += (nx + 1) / 2;
		}
		else {
			//std::cout << "j = " << j << " i = " << i << " k = " << k << " t = " << t   <<std::endl;
			while (k < count) {
				//A31u1->u[k] = -1 * (u1->u[i + nx - (nx - 1) * t / 2] + u1->u[i + nx - (nx - 1) * (t - 1) / 2 + 1]);
				A31u1->u[k] = -1 * u1->u[i  - (nx - 1) * t / 2] + -1*u1->u[i + (nx + 1) / 2 - (nx - 1) * t / 2];
				i++;
				k++;
			}
			j++;
			count += (nx - 1) / 2;
			//t++;
		}
	}
	return 0;
}

int Tetta1D31u3just(justVector* u3, MyDiagMatr2DStruct* matr, justVector* Tetta1D31u3, double tetta) {
	for (int i = 0; i < Tetta1D31u3->n; i++) {
		Tetta1D31u3->u[i] = -2 * tetta * u3->u[i];
	}
	return 0;
}
justVector Tetta1D31u3(MyVectorStruct* u, double tetta) {
	justVector res;
	int nx = u->nx;
	Init_justVector((nx * nx - 1) / 2, &res);
	for (int i = 0; i < res.n; i++) {
		res.u[i] = -2*tetta*u->u[nx + (nx * nx - 1) / 2 + 1 + i];
	}
	return res;
}

justVector f3(MyVectorStruct* f, justVector* Tetta1D31u3, justVector* A31u1) {
	justVector res;
	int nx = f->nx;
	Init_justVector((nx * nx - 1) / 2, &res);
	for (int i = 0; i < res.n; i++) {
		res.u[i] = (f->u[nx + (nx * nx - 1) / 2 + 1 + i] - Tetta1D31u3->u[i] - A31u1->u[i])/6;
	}
	return res;
}
justVector A23f3(justVector* f3) {
	int nx = sqrt(2*f3->n + 1);
	justVector res;
	Init_justVector((nx-1)*(nx - 1) /4, &res);
	int k = 0;
	
	for (int i = 0; i < res.n; i++) {
		if (i != 0 && i % ((nx - 1) / 2) == 0) {
			k += (nx + 1) / 2;
		}
		res.u[i] = -1*(-f3->u[i+k] - f3->u[i+ (nx - 1) / 2+k] - f3->u[i + (nx - 1) / 2 + 1+k] - f3->u[i+nx+k]);
	}
	return res;


}

int makeA23f3f3_new_div16(justVector* f3_new_div16, justVector* A23f3f3_new_div16) {
	int nx = sqrt(2 * f3_new_div16->n + 1);
	int k = 0;

	for (int i = 0; i < A23f3f3_new_div16->n; i++) {
		if (i != 0 && i % ((nx - 1) / 2) == 0) {
			k += (nx + 1) / 2;
		}
		A23f3f3_new_div16->u[i] = -f3_new_div16->u[i + k] - f3_new_div16->u[i + (nx - 1) / 2 + k] - f3_new_div16->u[i + (nx - 1) / 2 + 1 + k] - f3_new_div16->u[i + nx + k];
	}
	return 0;
}


int makef2_new(justVector* f2, justVector* A23f3f3_new_div16, justVector* f2_new) {
	for (int i = 0; i < f2->n; i++) {
		f2_new->u[i] = f2->u[i] - A23f3f3_new_div16->u[i];
	}
	return 0;
}

MyVectorStruct newF2(MyVectorStruct* f, justVector* other) {
	MyVectorStruct res;
	int nx = 2*sqrt(other->n) + 1;
	Init_MyVectorStruct((nx-1)/2, (nx - 1) / 2, &res);
	Init_MyZeroVector(&res);
	int i = 0;
	int k = 0;
	for (; i < res.nx; i++) {
		res.u[i] = 0;
	}

	for (; i < res.nx*res.ny + res.nx; i++) {
		//std::cout << "step = " << nx + (nx + 1) * (nx + 1) / 4 + i << " i = " << i<< " f []" << f->u[nx + (nx + 1) * (nx + 1) / 4 + i - res.nx]  << std::endl;
		res.u[i] = f->u[nx + (nx + 1) * (nx + 1) / 4 + i - res.nx] - other->u[i - res.nx];
	}

	for (; i < res.nx * res.ny + 2*res.nx; i++) {
		res.u[i] = 0;
	}
	return res;
}

int MyConstr_MatrReductionForCircle(MyDiagMatr2DStruct* DMG, double tetta) {
	int nx = DMG->nx;
	int ny = DMG->ny;
	int nd = DMG->nd;
	double* aa = DMG->a;
	for (int i = 0; i < 3 * nx - 2; i++) {
		aa[i] = 0.0;
	}
	int i = 0;
	int coun = nx;
	while (i < nd * nx) {
		if (i % nd * nx == 0 && i < nd) {
			aa[i + 3 * nx - 2] = 20./(4 + 2 * tetta);
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			aa[i + 3 * nx - 1] = 0.;
			aa[i + 3 * nx - 0] = -1. / (4 + 2*tetta);
			i++;
			i++;
			i++;
			coun = nx;
			continue;
		}
		aa[i + 3 * nx - 2] = -1. / (4 + 2*tetta);
		i++;
	}

	while (i < nd * nx * (ny - 1)) {
		if (i % nd * nx == 0 && coun == nx) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			aa[i + 3 * nx - 1] = 0.;
			aa[i + 3 * nx - 0] = -1. / (4 + 2*tetta);
			i++;
			i++;
			i++;
			coun = nx;
			continue;
		}
		aa[i + 3 * nx - 2] = -1. / (4 + 2*tetta);
		i++;
	}
	while (i < nd * nx * ny) {
		if (i % nd * nx == 0 && coun == nx) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun > 1) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			i++;
			coun--;
			continue;
		}
		if (i % nd == 0 && coun == 1) {
			aa[i + 3 * nx - 2] = 20. / (4 + 2 * tetta);
			aa[i + 3 * nx - 1] = 0.;
			aa[i + 3 * nx - 0] = -1. / (4 + 2*tetta);
			i++;
			i++;
			i++;
			coun = nx;
			continue;
		}
		aa[i + 3 * nx - 2] = -1. / (4 + 2*tetta);
		i++;
	}

	return 0;
}


justVector FromVectortoJust(MyVectorStruct* v) {
	int nx = v->nx;
	justVector res;
	Init_justVector(nx * nx, &res);
	for (int i = nx; i < nx * nx + nx; i++) {
		res.u[i - nx] = v->u[i];
	}
	return res;
}

MyVectorStruct FromJusttoVector(justVector* just) {
	MyVectorStruct res;
	Init_MyVectorStruct(sqrt(just->n), sqrt(just->n), &res);
	Init_MyZeroVector(&res);
	for (int i = 0; i < just->n; i++) {
		res.u[i + res.nx] = just->u[i];
	}
	return res;
}



int from5DuToBDu(MyVectorStruct* u5D, MyVectorStruct* uBD) {
	int k = 0;
	int j = 0;
	int i = 0;
	int nx = u5D->nx;

	while (k < (nx + 1) * (nx + 1) / 4) {
		if (j % 2 == 0) {
			while (i<nx*(j+1)) {
				uBD->u[k + nx] = u5D->u[i + nx];
				k++;
				i += 2;
			}
			j++;
			continue;
		}
		else{
			i = i - 1 + nx;
			j++;
			continue;
		}
	}
	j = 0;
	i = 0;
	while (k < (nx + 1) * (nx + 1) / 4 + (nx - 1) * (nx - 1) / 4)  {
		if (j % 2 == 0) {
			j++;
			i += nx + 1;
			continue;
		}
		else {
			while (i < nx * (j + 1)) {
				uBD->u[k + nx] = u5D->u[i + nx];
				k++;
				i += 2;
			}
			j++;
			continue;
		}
	}
	i = 1;
	while (k < nx * nx) {
		uBD->u[k + nx] = u5D->u[i + nx];
		k++;
		i += 2;
	}
	return 0;
}

int fromDBuTo5Du(MyVectorStruct* uBD, MyVectorStruct* u5D){
	int k = 0;
	int j = 0;
	int i = 0;
	int nx = u5D->nx;

	while (k < (nx + 1) * (nx + 1) / 4) {
		if (j % 2 == 0) {
			while (i < nx * (j + 1)) {
				u5D->u[i + nx] = uBD->u[k + nx];
				k++;
				i+=2;
			}
			j++;
			continue;
		}
		else {
			i = i - 1 + nx;
			j++;
			continue;
		}
	}
	j = 0;
	i = 0;
	while (k < (nx + 1) * (nx + 1) / 4 + (nx - 1) * (nx - 1) / 4) {
		if (j % 2 == 0) {
			j++;
			i += nx + 1;
			continue;
		}
		else {
			while (i < nx * (j + 1)) {
				u5D->u[i + nx] = uBD->u[k + nx];
				k++;
				i += 2;
			}
			j++;
			continue;
		}
	}
	i = 1;
	while (k < nx * nx) {
		u5D->u[i + nx] = uBD->u[k + nx];
		k++;
		i += 2;
	}
	return 0;
}



justVector from5DfToBDfANDdiv16(MyVectorStruct* fD5){
	justVector res;
	int nx = fD5->nx;
	Init_justVector((nx+1)*(nx+1)/4 + (nx - 1) * (nx - 1) / 4 - 1, &res);
	for (int i = 0; i < res.n; i++) {
		res.u[i] = fD5->u[(nx + 1) * (nx + 1) / 4 + (nx - 1) * (nx - 1) / 4 +nx +i]/6;
	}
	return res;
}

int makef3_new_div16(justVector* f3_new, double tetta) {
	for (int i = 0; i < f3_new->n; i++) {
		f3_new->u[i] = f3_new->u[i] / (4.+2*tetta);
	}
	return 0;
}

int updateFromVectorToJust(MyVectorStruct* v, justVector* just){
	int nx = v->nx;
	for (int i = 0; i < just->n; i++) {
		just->u[i] = v->u[i + nx];
	}
	return 0;
}

int glueVectorU(MyVectorStruct* u1u2u3, justVector* u1, justVector* u2, justVector* u3) {
	int nx = u1u2u3->nx;
	int i = 0;
	int k = 0;
	for (i; i < u1->n; i++) {
		u1u2u3->u[i + nx] = u1->u[k];
		k++;
	}
	k = 0;
	for (i; i < u1->n + u2->n; i++) {
		u1u2u3->u[i + nx] = u2->u[k];
		k++;
	}
	k = 0;
	for (i; i < u1->n + u2->n + u3->n; i++) {
		u1u2u3->u[i + nx] = u3->u[k];
		k++;
	}
	return 0;
}

int UnglueVectorU(MyVectorStruct* u1u2u3, justVector* u1, justVector* u2, justVector* u3) {
	int nx = u1u2u3->nx;
	int i = 0;
	int k = 0;
	for (i; i < u1->n; i++) {
		u1->u[k] = u1u2u3->u[i + nx];
		k++;
	}
	k = 0;
	for (i; i < u1->n + u2->n; i++) {
		u2->u[k] = u1u2u3->u[i + nx];
		k++;
	}
	k = 0;
	for (i; i < u1->n + u2->n + u3->n; i++) {
		u3->u[k] = u1u2u3->u[i + nx];
		k++;
	}
	return 0;
}

int makef3_new(justVector* f3, justVector* A31u1, justVector* Tetta1D31u3, justVector* f3_new) {
	for (int i = 0; i < f3->n; i++) {
		f3_new->u[i] = f3->u[i] - A31u1->u[i] - Tetta1D31u3->u[i];
	}
	return 0;
}