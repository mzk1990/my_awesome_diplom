#ifndef cheb
#define cheb
#pragma once
#include "matrix_struct.h"

typedef struct MyChebStruct
{
	double lamda_max;
	double lamda_min;
	int iter_max;
	long double* gamma_n;
	long double* alpha_n;
	long double* beta_n;

} MyChebStruct;

typedef struct MyChebStructCircly
{
	double lamda_max;
	double lamda_min;
	int iter_max;
	double* T_n;
	int N;

} MyChebStructCircly;


int Init_ChebStruct(MyChebStruct* my_cheb, double lamda_max, double lamda_min, int iter_max);
int ConstrCheb(MyChebStruct* my_cheb);
int CalculateCheba(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res);
int CalculateChebaOneStep(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res);
int CalculateChebaRichards(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res);
int* giveParam(int N);
void coef(int* a, int* b, int fn, int ln);
int Init_ChebStructCircly(MyChebStructCircly* my_cheb, double lamda_max, double lamda_min, int iter_max, int N);
int ConstrChebCircly(MyChebStructCircly* my_cheb);
int ConstrChebCirclyNotOrdered(MyChebStructCircly* my_cheb);
int CalculateChebaCirclyNotOrdered(MyChebStructCircly* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res);
int CalculateHalfStatic(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res);
MyVectorStruct find_u2(MyVectorStruct* init_u, MyDiagMatr2DStruct* matr);

int find_u1u2u3(MyDiagMatr2DStruct* matr, MyVectorStruct* u0, MyVectorStruct* f5D, int iterm, double eps, double tetta);

#endif //cheb
