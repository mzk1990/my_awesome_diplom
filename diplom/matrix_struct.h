#ifndef matrix_struct
#define matrix_struct


typedef struct MyDiagMatr2DStruct
{
	int nx;
	int ny;
	int nd;
	double* a;
} MyDiagMatr2DStruct;

typedef struct justVector
{
	int n;
	double* u;

} justVector;

int Init_justVector(int n, justVector* v);

typedef struct MyVectorStruct
{
	int nx;
	int ny;
	double* u;
} MyVectorStruct;

int Init_MyVectorStruct(
	int nx,				// Number of nodes along the axis X
	int ny,				// Number of nodes along the axis Y
	MyVectorStruct* U // Structure for store for the matrix in dioganal format
	);
int Init_MyEyeVector(MyVectorStruct* U);
int Init_MyTooEyeVector(MyVectorStruct* U);
int Init_MyZeroVector(MyVectorStruct* U);
int Init_MyReductionVector(MyVectorStruct* U);
int Init_MyRandomVector(MyVectorStruct* U);
int Init_MyEye7Vector(MyVectorStruct* U);
int Init_My123Vector(MyVectorStruct* U);

justVector from_Full_u_To_Reduction_u(MyVectorStruct* U);
justVector from_Full_u_To_Reduction_u2(MyVectorStruct* U);

MyVectorStruct fromFulluToReductionu(MyVectorStruct* U);

MyVectorStruct fromReductionuToFullu(MyVectorStruct* U, MyVectorStruct* f, MyDiagMatr2DStruct* DMG);

int ReductionMatrixAndVectorF(MyDiagMatr2DStruct *full_m, MyDiagMatr2DStruct *reduction_m, MyVectorStruct* full_f, MyVectorStruct* reduction_f, justVector* reduction_u);

int My_Myltiplication_MonV(MyDiagMatr2DStruct* DMG, MyVectorStruct* U, MyVectorStruct* res);

int Init_MyDiagMatr2DStruct(
	int nx,				// Number of nodes along the axis X
	int ny,				// Number of nodes along the axis Y
	int nd,				// Number of diagonals
	MyDiagMatr2DStruct* DMG // Structure for store for the matrix in dioganal format
	);

int MyConstr_Matr_2D5DS_RD(
	MyDiagMatr2DStruct* DMG // Structure for store for the matrix in dioganal format
	);
int MyConstr_MatrReduction(MyDiagMatr2DStruct* DMG);


int VectorSub(MyVectorStruct* v1,MyVectorStruct* v2, MyVectorStruct* res);
double VectorScalar(MyVectorStruct* v1, MyVectorStruct* v2);
int VectorMyltiplicationOnScalar(MyVectorStruct* v1, double scalar);
int VectorPlus(MyVectorStruct* v1, MyVectorStruct* v2, MyVectorStruct* res);
int VectorEqual(MyVectorStruct* v1, MyVectorStruct* v2);
int ZeroJustVector(justVector* just);
MyVectorStruct Prolongation(MyVectorStruct* v, MyDiagMatr2DStruct* matr, MyVectorStruct* f);

double VectorError(MyVectorStruct* v1, MyVectorStruct* v2);
int Init_sinxsiny(MyVectorStruct* U);

MyVectorStruct ProlongationVector(MyVectorStruct* v, justVector* redus);
justVector A31u1(MyVectorStruct* u);
justVector Tetta1D31u3(MyVectorStruct* u, double tetta);
justVector f3(MyVectorStruct* f, justVector* Tetta1D31u3, justVector* A31u1);
justVector A23f3(justVector* f3);
int A31u1just(justVector* u1, MyDiagMatr2DStruct* matr, justVector* A31u1);  // работает
int Tetta1D31u3just(justVector* u3, MyDiagMatr2DStruct* matr, justVector* Tetta1D31u3, double tetta);
int makef3_new(justVector* f3, justVector* A31u1, justVector* Tetta1D31u3, justVector* f3_new);
int makef3_new_div16(justVector* f3_new, double tetta);
int makeA23f3f3_new_div16(justVector* f3_new_div16, justVector* A23f3f3_new_div16);
int makef2_new(justVector* f2, justVector* A23f3f3_new_div16, justVector* f2_new);
MyVectorStruct newF2(MyVectorStruct* f, justVector* other);


int MyConstr_MatrReductionForCircle(MyDiagMatr2DStruct* DMG, double tetta);

justVector FromVectortoJust(MyVectorStruct* v);

MyVectorStruct FromJusttoVector(justVector* just);

int updateFromVectorToJust(MyVectorStruct* v, justVector* just);

int from5DuToBDu(MyVectorStruct* u5D, MyVectorStruct* uBD);
int fromDBuTo5Du(MyVectorStruct* uBD, MyVectorStruct* u5D);

justVector from5DfToBDfANDdiv16(MyVectorStruct* fD5);

int glueVectorU(MyVectorStruct* u1u2u3, justVector* u1, justVector* u2, justVector* u3);

int UnglueVectorU(MyVectorStruct* u1u2u3, justVector* u1, justVector* u2, justVector* u3);


#endif //matrix_struct
