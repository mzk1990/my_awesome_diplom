#pragma once

#include "cheb.h"
#include "matrix_struct.h"
#include <stdlib.h>
#include <iostream>
#define PI 3.1415926535897932384626433832795

int Init_ChebStruct(MyChebStruct* my_cheb, double lamda_max, double lamda_min, int iter_max) {
	my_cheb->lamda_max = lamda_max;
	my_cheb->lamda_min = lamda_min;
	my_cheb->iter_max = iter_max;
	my_cheb->gamma_n = (long double*)malloc(iter_max * sizeof(long double));
	my_cheb->alpha_n = (long double*)malloc(iter_max * sizeof(long double));
	my_cheb->beta_n = (long double*)malloc(iter_max * sizeof(long double));

	return 0;
}

int Init_ChebStructCircly(MyChebStructCircly* my_cheb, double lamda_max, double lamda_min, int iter_max, int N) {
	my_cheb->lamda_max = lamda_max;
	my_cheb->lamda_min = lamda_min;
	my_cheb->iter_max = iter_max;
	my_cheb->T_n = (double*)malloc((N + 1) * sizeof(double));
	my_cheb->N = N;
	return 0;
}


int ConstrChebCircly(MyChebStructCircly* my_cheb) {
	double* index = my_cheb->T_n;
	int N = my_cheb->N;
	double lmax = my_cheb->lamda_max;
	double lmin = my_cheb->lamda_min;
	int* Tn = giveParam(N);

	for (int n = 1; n <= N; n++) {

		index[n] = 2.0 / (lmax + lmin - (lmax - lmin) * (cos((2 * (double)Tn[n] -1) * (double)PI / ( 2 * (double)N))));
	}
	
	//for (int i = 0; i < N + 1; i++) {
	//	index[i] = 0;
	//	index[i] = index[i + 1];
	//}
	free(Tn);
	return 0;
}

int ConstrChebCirclyNotOrdered(MyChebStructCircly* my_cheb) {
	double* index = my_cheb->T_n;
	int N = my_cheb->N;
	double lmax = my_cheb->lamda_max;
	double lmin = my_cheb->lamda_min;

	for (int n = 1; n <= N; n++) {

		index[n] = 2.0 / (lmax + lmin - (lmax - lmin) * (cos((2 * n - 1) * (double)PI / (2 * (double)N))));
	}

	return 0;
}




int ConstrCheb(MyChebStruct* my_cheb) {
	long double* g = my_cheb->gamma_n;
	long double* a = my_cheb->alpha_n;
	long double* b = my_cheb->beta_n;
	double gamma = (my_cheb->lamda_max + my_cheb->lamda_min) / (my_cheb->lamda_max - my_cheb->lamda_min);
	double delta_last = 1 / gamma;
	double delta_next = 1 / (2 * gamma - delta_last);
	g[0] = 0;
	a[0] = 2. / (my_cheb->lamda_max + my_cheb->lamda_min);
	b[0] = 0;
	int k = 0;
	for (int i = 1; i < my_cheb->iter_max; ) {
		g[i] = delta_next * delta_last;
		//g[i] = delta_next / delta_last;
		delta_last = delta_next;
		delta_next = 1 / (2 * gamma - delta_last);
		if (k < 10) {
			k++;
			//std::cout << "dl = " << delta_last << " dn = " << delta_next << std::endl;
		}
		a[i] = (2*(1+g[i])) / (my_cheb->lamda_max + my_cheb->lamda_min);
		b[i] = g[i] * a[i - 1] / a[i];
		i++;
	}
	return 0;
}


// ��� ����������
int CalculateCheba(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res ) {
	MyVectorStruct un;
	MyVectorStruct rn;
	MyVectorStruct pn;
	int nx = DMG->nx;
	int ny = DMG->ny;
	long double* g = my_cheb->gamma_n;
	long double* a = my_cheb->alpha_n;
	long double* b = my_cheb->beta_n;
	MyVectorStruct support1;
	MyVectorStruct support2;
	MyVectorStruct support3;
	int count = 0;
	int MaxIter = my_cheb->iter_max;
	int i = 1;
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &rn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &pn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support3);
	Init_MyZeroVector(&un);
	Init_MyZeroVector(&rn);
	Init_MyZeroVector(&pn);
	Init_MyZeroVector(&support1);
	Init_MyZeroVector(&support2);
	Init_MyZeroVector(&support3);
	
	My_Myltiplication_MonV(DMG,&un,&support1);// support1 = A*un
	VectorSub(f, &support1, &support2);		  // support2 = f - A*un
	VectorEqual(&rn, &support2);			  // rn = support2
	VectorEqual(&pn, &rn);					  // pn = rn

	while ((VectorScalar(&rn,&rn)* (VectorScalar(&rn, &rn) >= eps*eps*VectorScalar(f, f) * VectorScalar(f, f)))&&count < MaxIter) {
		VectorEqual(&support2, &pn);                   // support2 = pn
		VectorMyltiplicationOnScalar(&pn, a[i - 1]);   // pn = pn*a[i - 1]
		VectorPlus(&un,&pn,&support1);                 // support1 = un + pn
		VectorEqual(&un, &support1);                   // un = support1
		My_Myltiplication_MonV(DMG, &pn, &support1);   // support1 = A*pn
		VectorSub(&rn, &support1, &support3);          // support3 = rn - support1
		VectorEqual(&rn, &support3);                   // rn = support3
		VectorMyltiplicationOnScalar(&support2, b[i]); // support2 = support2*b[i]
		VectorPlus(&rn, &support2, &support1);         // support1 = rn + support2
		VectorEqual(&pn, &support1);                   // pn = support1
		//for (int i = 0; i < nx * ny + 2 * nx; i++) {
		//	std::cout << i << "   " << un.u[i] << std::endl;
		//}
		i++;
		count++;
		
	}



	VectorEqual(res, &un);

	free(un.u);
	free(rn.u);
	free(pn.u);
	free(support1.u);
	free(support2.u);
	free(support3.u);
	return count;
}


// �����������
int CalculateChebaOneStep(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res) {
	MyVectorStruct un0;
	MyVectorStruct un1;
	MyVectorStruct un2;
	MyVectorStruct rn;
	MyVectorStruct pn;
	int nx = DMG->nx;
	int ny = DMG->ny;
	long double* g = my_cheb->gamma_n;
	long double* a = my_cheb->alpha_n;
	long double* b = my_cheb->beta_n;
	MyVectorStruct support1;
	MyVectorStruct support2;
	MyVectorStruct support3;
	int count = 0;
	int MaxIter = my_cheb->iter_max;
	int i = 1;
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un0);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &rn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &pn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support3);
	Init_MyZeroVector(&un0); // un0 = 0
	Init_MyZeroVector(&un1); // un1 = 0
	Init_MyZeroVector(&un2); // un2 = 0
	Init_MyZeroVector(&rn);
	Init_MyZeroVector(&pn);
	Init_MyZeroVector(&support1);
	Init_MyZeroVector(&support2);
	Init_MyZeroVector(&support3);
	//VectorEqual(&un0, res);


	My_Myltiplication_MonV(DMG, &un0, &support1);	// support1 = A*un0
	VectorSub(f, &support1, &support2);				// support2 = f - support1
	VectorEqual(&rn, &support2);					// rn = support2
	VectorMyltiplicationOnScalar(&support2, a[0]);	// support2 = support2*a[0]
	VectorPlus(&un0, &support2, &un1);				// un1 = un0+support2

	while ((VectorScalar(&rn, &rn) >=  eps *  VectorScalar(f, f)) && count < MaxIter) {
		//VectorMyltiplicationOnScalar(&rn, a[i]);			// rn = rn*a[i]
		//VectorSub(&un1, &un0, &support1);					// support1 = un1 - un0
		//VectorMyltiplicationOnScalar(&support1, g[i]);		// support1 = support1*g[i]
		//VectorPlus(&support1, &rn, &support2);				// support2 = support1 + rn
		//VectorPlus(&support2, &un1, &support3);				// support3 = support2 + un1
		//VectorEqual(&un0, &un1);							// un0 = un1
		//VectorEqual(&un1, &support3);						// un1 = support3
		//My_Myltiplication_MonV(DMG, &un1, &support1);		// support1 = A*un1
		//VectorSub(f, &support1, &support2);					// support2 = f - support1
		//VectorEqual(&rn, &support2);						// rn = support2


		My_Myltiplication_MonV(DMG, &un1, &support1);		// support1 = A*un1
		VectorSub(f, &support1, &support2);					// support2 = f - support1
		VectorEqual(&rn, &support2);						// rn = support2
		VectorMyltiplicationOnScalar(&rn, a[i]);			// rn = rn*a[i]
		VectorSub(&un1, &un0, &support1);					// support1 = un1 - un0
		VectorMyltiplicationOnScalar(&support1, g[i]);		// support1 = support1*g[i]
		VectorPlus(&support1, &rn, &support2);				// support2 = support1 + rn
		VectorPlus(&support2, &un1, &support3);				// support3 = support2 + un1
		VectorEqual(&un0, &un1);							// un0 = un1
		VectorEqual(&un1, &support3);						// un1 = support3
		i++;
		count++;
	}



	VectorEqual(res, &un1);

	free(un1.u);
	free(rn.u);
	free(pn.u);
	free(support1.u);
	free(support2.u);
	free(support3.u);
	return count;
}


int CalculateHalfStatic(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res) {
	MyVectorStruct un0;
	MyVectorStruct un1;
	MyVectorStruct un2;
	MyVectorStruct rn;
	MyVectorStruct pn;
	int nx = DMG->nx;
	int ny = DMG->ny;
	MyVectorStruct support1;
	MyVectorStruct support2;
	MyVectorStruct support3;
	MyVectorStruct support4;
	int count = 0;
	int MaxIter = my_cheb->iter_max;
	int i = 1;
	double ak = 2.0;
	double ro = (1 - (my_cheb->lamda_min / my_cheb->lamda_max)) / (1 + (my_cheb->lamda_min / my_cheb->lamda_max));
	double tau = 2 / (my_cheb->lamda_max + my_cheb->lamda_min);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un0);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &rn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &pn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support3);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support4);
	Init_MyZeroVector(&un0); // un0 = 0
	Init_MyZeroVector(&un1); // un1 = 0
	Init_MyZeroVector(&un2); // un2 = 0
	Init_MyZeroVector(&rn);
	Init_MyZeroVector(&pn);
	Init_MyZeroVector(&support1);
	Init_MyZeroVector(&support2);
	Init_MyZeroVector(&support3);
	Init_MyZeroVector(&support4);


	VectorEqual(&support2, f);						//sup2 = f
	VectorMyltiplicationOnScalar(&support2, tau);	//sup2 = sup2*tau
	VectorEqual(&un1, &support2);					//un1 = sup2
	VectorEqual(&support2, f);						//sup2 = f
	VectorMyltiplicationOnScalar(&support2, -1);	//sup2 = -1*f
	VectorEqual(&rn, &support2);					//rn = sup2

	while ((VectorScalar(&rn, &rn) >= eps * eps * VectorScalar(f, f)) && count < MaxIter) {
		ak = 4 / (4 - ro * ro * ak);
		double t1 = 1 - ak;
		double t2 = -1*ak*tau;
		//std::cout << ak << std::endl;
		VectorEqual(&support2, &un1);						//sup2 = un1
		VectorMyltiplicationOnScalar(&support2, ak);		//sup2 = sup2*ak
		VectorEqual(&support1, &un0);						//sup1 = un0
		VectorMyltiplicationOnScalar(&support1, t1);		//sup1 = sup2*(1-ak)
		VectorEqual(&support3, &rn);						//sup3 = rn
		VectorMyltiplicationOnScalar(&support3, t2);		//sup2 = sup2*ak
		VectorPlus(&support1, &support2, &support4);			//sup4 = sup1+sup2
		VectorPlus(&support4, &support3, &un1);				// un1 = sup4+sup3
		My_Myltiplication_MonV(DMG, &un1, &support1);		// support1 = A*un1
		VectorSub(&support1, f, &support2);					// support2 = support1 - f
		//VectorSub(f, &support1, &support2);					// support2 = -support1 + f
		VectorEqual(&rn, &support2);						// rn = sup2
		i++;
		count++;
	}



	VectorEqual(res, &un1);

	free(un1.u);
	free(rn.u);
	free(pn.u);
	free(support1.u);
	free(support2.u);
	free(support3.u);
	return count;
}


// ���������
int CalculateChebaRichards(MyChebStruct* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res) {
	MyVectorStruct un;
	MyVectorStruct rn;
	MyVectorStruct pn;
	int nx = DMG->nx;
	int ny = DMG->ny;
	long double* g = my_cheb->gamma_n;
	long double* a = my_cheb->alpha_n;
	long double* b = my_cheb->beta_n;
	MyVectorStruct support1;
	MyVectorStruct support2;
	MyVectorStruct support3;
	int count = 0;
	int MaxIter = my_cheb->iter_max;
	int i = 1;
	double tau = 2 / (my_cheb->lamda_max + my_cheb->lamda_min);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &rn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &pn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support3);
	Init_MyZeroVector(&un); // un = 0
	Init_MyZeroVector(&rn);
	Init_MyZeroVector(&pn);
	Init_MyZeroVector(&support1);
	Init_MyZeroVector(&support2);
	Init_MyZeroVector(&support3);
	My_Myltiplication_MonV(DMG, &un, &support1);	// support1 = A*un
	VectorSub(f, &support1, &support2);				// support2 = f - A*un
	VectorEqual(&rn, &support2);					// rn = support2

	while ((VectorScalar(&rn, &rn) >= eps * eps * VectorScalar(f, f)) && count < MaxIter) {

		VectorMyltiplicationOnScalar(&support2, tau);   // support2 = tau*support2
		VectorPlus(&un, &support2, &support1);          // support1 = un + support2
		VectorEqual(&un, &support1);                    // un = support1
		My_Myltiplication_MonV(DMG, &un, &support1);	// support1 = A*un
		VectorSub(f, &support1, &support2);				// support2 = f - A*un
		VectorEqual(&rn, &support2);					// rn = support2
		i++;
		count++;
	}



	VectorEqual(res, &un);

	free(un.u);
	free(rn.u);
	free(support1.u);
	free(support2.u);
	return count;
}

int CalculateChebaCirclyNotOrdered(MyChebStructCircly* my_cheb, MyDiagMatr2DStruct* DMG, MyVectorStruct* f, double eps, MyVectorStruct* res) {
	MyVectorStruct un;
	MyVectorStruct rn;
	MyVectorStruct pn;
	int nx = DMG->nx;
	int ny = DMG->ny;
	int N = my_cheb->N;
	MyVectorStruct support1;
	MyVectorStruct support2;
	MyVectorStruct support3;
	int count = 0;
	int MaxIter = my_cheb->iter_max;
	int i = 0;
	double* tau = my_cheb->T_n;
	double tau1 = 2 / (my_cheb->lamda_max + my_cheb->lamda_min);
	double tau2 = 0.126;
	Init_MyVectorStruct(DMG->nx, DMG->ny, &un);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &rn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &pn);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support1);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support2);
	Init_MyVectorStruct(DMG->nx, DMG->ny, &support3);
	Init_MyZeroVector(&un); // un = 0
	Init_MyZeroVector(&rn);
	Init_MyZeroVector(&pn);
	Init_MyZeroVector(&support1);
	Init_MyZeroVector(&support2);
	Init_MyZeroVector(&support3);
	My_Myltiplication_MonV(DMG, &un, &support1);	// support1 = A*un
	VectorSub(f, &support1, &support2);				// support2 = f - A*un
	VectorEqual(&rn, &support2);					// rn = support2

	while ((VectorScalar(&rn, &rn) >= eps * eps * VectorScalar(f, f)) && count < MaxIter) {
		VectorEqual(&support1, &rn);								// support1 = rn
		VectorMyltiplicationOnScalar(&support1, tau[i%N+1]);		// support1 = support1*tau
		VectorPlus(&un, &support1, &support2);						// support2 = un + support1
		VectorEqual(&un, &support2);								// un = support2
		My_Myltiplication_MonV(DMG, &rn, &support3);				// support3 = A*rn
		VectorMyltiplicationOnScalar(&support3, tau[i % N + 1]);	// support3 = support3*tau
		VectorSub(&rn, &support3, &support2);						// support2 = rn - support3
		VectorEqual(&rn, &support2);								// rn = support2
		i++;
		count++;
	}


	 
	VectorEqual(res, &un);

	free(un.u);
	free(rn.u);
	free(support1.u);
	free(support2.u);
	return count;
}
int* giveParam(int N) {
	int* a = (int*)malloc((N + 1) * sizeof(int));
	int* b = (int*)malloc((N + 1) * sizeof(int));
	int firstN = 2;
	a[1] = 1;
	a[2] = 3;
	coef(a, b, firstN, N);
	//free(b);
	return a;
}

void coef(int* a, int* b, int fn, int ln) {

	int j = 1;
	for (int i = 1; i < fn + 1; i++) {
		b[j] = a[i];
		b[j + 1] = fn * 4 - a[i];
		j = j + 2;
	}
	fn = fn * 2;
	for (int i = 1; i < fn + 1; i++) {
		a[i] = b[i];
	}
	if (fn == ln) {
		return;
	}
	coef(a, b, fn, ln);
}

MyVectorStruct find_u2(MyVectorStruct* f, MyDiagMatr2DStruct* matr) {
	int nx = f->nx;
	int ny = nx;
	MyVectorStruct zero_u;
	Init_MyVectorStruct(nx, ny, &zero_u);
	Init_MyEyeVector(&zero_u);
	justVector jtest;
	justVector jD31u3;
	justVector f31;
	justVector A23f31;
	MyVectorStruct newf2;
	MyDiagMatr2DStruct redtestM;
	MyChebStruct Reduction_cheba_test;
	Init_ChebStruct(&Reduction_cheba_test, 11. / 3, 8. / 3, 10000);
	ConstrCheb(&Reduction_cheba_test);
	Init_MyDiagMatr2DStruct((nx - 1) / 2, (nx - 1) / 2, 3, &redtestM);
	MyConstr_MatrReductionForCircle(&redtestM, 1.0);
	MyVectorStruct testresult_u;
	MyVectorStruct result_u;
	Init_MyVectorStruct((nx - 1) / 2, (nx - 1) / 2, &testresult_u);
	Init_MyVectorStruct((nx - 1) / 2, (nx - 1) / 2, &result_u);
	Init_MyZeroVector(&testresult_u);
	Init_MyZeroVector(&result_u);
	MyDiagMatr2DStruct reduc_matrix_test;
	MyVectorStruct reduc_f_test;
	MyVectorStruct baby_zero;
	Init_MyVectorStruct((nx + 1) / 2, (ny + 1) / 2, &baby_zero);
	Init_MyZeroVector(&baby_zero);
	MyChebStruct Reduction_cheba;
	Init_ChebStruct(&Reduction_cheba, 4.0, 2.0, 10000);
	ConstrCheb(&Reduction_cheba);
	MyVectorStruct testresult_u_longred;
	MyVectorStruct longred2343;

	int it = 0;
	while (it < 1) {
		//std::cout << "zero_u" << std::endl;
		for (int i = 0; i < zero_u.nx * zero_u.ny + 2 * zero_u.nx; i++) {
			//std::cout << i << " " << zero_u.u[i] << std::endl;
		}
		jtest = A31u1(&zero_u);
		//std::cout << "jtest" << std::endl;
		for (int i = 0; i < jtest.n; i++) {
		//	std::cout << i << " " << jtest.u[i] << std::endl;
		}
		jD31u3 = Tetta1D31u3(&zero_u, 1.);
		//std::cout << "jD31u3" << std::endl;
		for (int i = 0; i < jD31u3.n; i++) {
			//std::cout << i << " " << jD31u3.u[i] << std::endl;
		}
		f31 = f3(f, &jD31u3, &jtest);
		//std::cout << "f31" << std::endl;
		for (int i = 0; i < f31.n; i++) {
			//std::cout << i << " " << f31.u[i] << std::endl;
		}
		A23f31 = A23f3(&f31);
		//std::cout << "A23f31" << std::endl;
		for (int i = 0; i < A23f31.n; i++) {
			//std::cout << i << " " << A23f31.u[i] << std::endl;
		}
		newf2 = newF2(f, &A23f31);
		//std::cout << "newf2" << std::endl;
		for (int i = 0; i < newf2.nx * newf2.ny + 2 * newf2.nx; i++) {
			//std::cout << i << " " << newf2.u[i] << std::endl;
		}
		CalculateChebaOneStep(&Reduction_cheba_test, &redtestM, &newf2, 0.000001, &testresult_u);
		ReductionMatrixAndVectorF(matr, &reduc_matrix_test, f, &reduc_f_test, &FromVectortoJust(&testresult_u));
		CalculateChebaOneStep(&Reduction_cheba, &reduc_matrix_test, &reduc_f_test, 0.000001, &baby_zero);
		testresult_u_longred = ProlongationVector(&baby_zero, &FromVectortoJust(&testresult_u));
		longred2343 = Prolongation(&testresult_u_longred, matr, f);
		std::cout << std::endl;
		for (int i = 0; i < longred2343.nx * longred2343.ny + 2 * longred2343.nx; i++) {
			//std::cout << i << " " << longred2343.u[i] << std::endl;
		}
		VectorEqual(&zero_u ,&longred2343 );
		it++;
	}
	return longred2343;
}

int find_u1u2u3(MyDiagMatr2DStruct* matr, MyVectorStruct* u0, MyVectorStruct* f5D, int iterm, double eps, double tetta) {//u0 == 0
	int count = 0;
	int count_iter_BD = 0;
	int count_iter_5D = 0;
	int MaxIter = iterm;
	int nx = matr->nx;
	//double tetta = 1;
	MyVectorStruct fBD;
	MyVectorStruct u1u2u3BD;
	MyVectorStruct resultU5D;
	MyVectorStruct u1u2u35D;
	MyVectorStruct support1;
	MyVectorStruct rn;
	MyVectorStruct longred; // ��������� 2 ������� ������� ����� � ��������
	MyVectorStruct all_solve; // ����� ����� � �������� �������� ��������, ������ ������
	Init_MyVectorStruct(nx, nx, &fBD); 
	Init_MyVectorStruct(nx, nx, &u1u2u3BD);
	Init_MyVectorStruct(nx, nx, &u1u2u35D);
	Init_MyVectorStruct(nx, nx, &resultU5D);
	Init_MyZeroVector(&fBD);
	Init_MyZeroVector(&u1u2u3BD);
	Init_MyZeroVector(&u1u2u35D);
	Init_MyZeroVector(&resultU5D);
	justVector u1;
	justVector u2;
	justVector u3;
	justVector f1;
	justVector f2;
	justVector f2_new;
	justVector f3;
	justVector f3_new;
	justVector A31u1;
	justVector Tetta1D31u3;
	justVector A23f3f3_new_div16;
	justVector reduction_u1u2u3;
	MyVectorStruct baby_zero;
	MyVectorStruct test_res;
	Init_MyVectorStruct((nx - 1) / 2, (nx - 1) / 2, &test_res);
	Init_MyVectorStruct(nx, nx, &longred);
	Init_MyVectorStruct(nx, nx, &all_solve);
	Init_MyVectorStruct(nx, nx, &support1);
	Init_MyVectorStruct(nx, nx, &rn);
	Init_MyZeroVector(&test_res);
	Init_MyZeroVector(&support1);
	Init_MyZeroVector(&rn);
	Init_justVector((nx - 1) * (nx - 1) / 4, &A23f3f3_new_div16);
	Init_justVector((nx * nx - 1) / 2, &Tetta1D31u3);
	Init_justVector((nx * nx - 1) / 2, &A31u1);
	Init_justVector((nx + 1) * (nx + 1) / 4, &u1);  // ����� 
	Init_justVector((nx - 1) * (nx - 1) / 4, &u2); // ������ - ������ �������
	Init_justVector(nx * nx - (nx - 1) * (nx - 1) / 4 - (nx + 1) * (nx + 1) / 4, &u3); // ��������
	Init_justVector((nx + 1) * (nx + 1) / 4, &f1);  // ����� 
	Init_justVector((nx - 1) * (nx - 1) / 4, &f2); // ������ - ������ �������
	Init_justVector((nx - 1) * (nx - 1) / 4, &f2_new); // ������ - ������ �������
	Init_justVector(nx * nx - (nx - 1) * (nx - 1) / 4 - (nx + 1) * (nx + 1) / 4, &f3); // ��������
	Init_justVector(nx * nx - (nx - 1) * (nx - 1) / 4 - (nx + 1) * (nx + 1) / 4, &f3_new); // ��������
	Init_MyVectorStruct((nx + 1) / 2, (nx + 1) / 2, &baby_zero);

	MyDiagMatr2DStruct Reduction_matr_test;
	Init_MyDiagMatr2DStruct((nx - 1) / 2, (nx - 1) / 2, matr->nd, &Reduction_matr_test);
	MyConstr_MatrReductionForCircle(&Reduction_matr_test, tetta);

	MyChebStruct Reduction_cheba_test;
	Init_ChebStruct(&Reduction_cheba_test, 22. / (4+2*tetta), 16. / (4+2*tetta), iterm); //���� ��� BD
	ConstrCheb(&Reduction_cheba_test);

	MyChebStruct Reduction_cheba_for_D5;
	Init_ChebStruct(&Reduction_cheba_for_D5, 4, 2, iterm);
	ConstrCheb(&Reduction_cheba_for_D5);

	MyDiagMatr2DStruct reduc_m_test;
	MyVectorStruct reduc_f_test;


	from5DuToBDu(f5D, &fBD);

	//std::cout << "fBD  " << std::endl;
	//for (int i = 0; i < fBD.nx * fBD.nx + 2 * fBD.nx; i++) {
	//	std::cout << i << " " << fBD.u[i] << std::endl;
	//}

	UnglueVectorU(u0, &u1, &u2, &u3);

	//std::cout << "u1  " << std::endl;
	//for (int i = 0; i < u1.n; i++) {
	//	std::cout << i << " " << u1.u[i] << std::endl;
	//}
	//std::cout << "u2  " << std::endl;
	//for (int i = 0; i < u2.n; i++) {
	//	std::cout << i << " " << u2.u[i] << std::endl;
	//}
	//std::cout << "u3  " << std::endl;
	//for (int i = 0; i < u3.n; i++) {
	//	std::cout << i << " " << u3.u[i] << std::endl;
	//}

	UnglueVectorU(&fBD, &f1, &f2, &f3);

	//std::cout << "f1  " << std::endl;
	//for (int i = 0; i < f1.n; i++) {
	//	std::cout << i << " " << f1.u[i] << std::endl;
	//}
	//std::cout << "f2  " << std::endl;
	//for (int i = 0; i < f2.n; i++) {
	//	std::cout << i << " " << f2.u[i] << std::endl;
	//}
	//std::cout << "f3  " << std::endl;
	//for (int i = 0; i < f3.n; i++) {
	//	std::cout << i << " " << f3.u[i] << std::endl;
	//}

	fromDBuTo5Du(u0, &resultU5D);

	//for (int i = 0; i < u0->nx * u0->ny + 2 * u0->nx; i++) {
	//	std::cout << i << " " << resultU5D.u[i] << std::endl;
	//}

	My_Myltiplication_MonV(matr, &resultU5D, &support1);

	//std::cout << "support1" << std::endl;
	//for (int i = 0; i < support1.nx * support1.ny + 2 * support1.nx; i++) {
	//	std::cout << support1.u[i] << std::endl;
	//}

	VectorSub(&support1, f5D, &rn);

	//std::cout << "rn" << std::endl;
	//for (int i = 0; i < support1.nx * support1.ny + 2 * support1.nx; i++) {
	//	std::cout << rn.u[i] << std::endl;
	//}
	std::cout << std::endl;
	std::cout <<"tetta ="  << tetta<<std::endl;
	while ((VectorScalar(&rn, &rn) >= eps * VectorScalar(f5D, f5D)) && count < MaxIter) {
		
		std::cout <<count<< "|rn| = " << VectorScalar(&rn, &rn) << std::endl;
		count++;
		A31u1just(&u1, matr, &A31u1);

		//std::cout << "A31u1  " << std::endl;
		//for (int i = 0; i < A31u1.n; i++) {
		//	std::cout << i << " " << A31u1.u[i] << std::endl;
		//}

		Tetta1D31u3just(&u3, matr, &Tetta1D31u3, tetta);

		//std::cout << "Tetta1D31u3  " << std::endl;

		//for (int i = 0; i < Tetta1D31u3.n; i++) {
		//	std::cout << i << " " << Tetta1D31u3.u[i] << std::endl;
		//}

		makef3_new(&f3, &A31u1, &Tetta1D31u3, &f3_new);

		//std::cout << "f3_new  " << std::endl;
		//for (int i = 0; i < f3_new.n; i++) {
		//	std::cout << i << " " << f3_new.u[i] << std::endl;
		//}

		makef3_new_div16(&f3_new, tetta);

		//std::cout << "f3_new_div16  " << std::endl;
		//for (int i = 0; i < f3_new.n; i++) {
		//	std::cout << i << " " << f3_new.u[i] << std::endl;
		//}

		makeA23f3f3_new_div16(&f3_new, &A23f3f3_new_div16);

		//std::cout << "A23f3f3_new_div16  " << std::endl;
		//for (int i = 0; i < A23f3f3_new_div16.n; i++) {
		//	std::cout << i << " " << A23f3f3_new_div16.u[i] << std::endl;
		//}

		makef2_new(&f2, &A23f3f3_new_div16, &f2_new);

		//std::cout << "f2_new  " << std::endl;
		//for (int i = 0; i < f2_new.n; i++) {
		//	std::cout << i << " " << f2_new.u[i] << std::endl;
		//}

		MyVectorStruct f2_newVector = FromJusttoVector(&f2_new);

		//std::cout << "f2_newVector  " << std::endl;
		//for (int i = 0; i < f2_newVector.nx * f2_newVector.nx + 2 * f2_newVector.nx; i++) {
		//	std::cout << i << " " << f2_newVector.u[i] << std::endl;
		//}

		count_iter_BD += CalculateChebaOneStep(&Reduction_cheba_test, &Reduction_matr_test, &FromJusttoVector(&f2_new), eps, &test_res);
		
		//std::cout << "test_iter  " << count_iter_BD << std::endl;
		//for (int i = 0; i < test_res.nx * test_res.nx + 2 * test_res.nx; i++) {
		//	std::cout << i << " " << test_res.u[i] << std::endl;
		//}

		updateFromVectorToJust(&test_res, &u2);

		//std::cout << "u2" << std::endl;
		//for (int i = 0; i < u2.n; i++) {
		//	std::cout << i << " " << u2.u[i] << std::endl;
		//}

		glueVectorU(&u1u2u3BD, &u1, &u2, &u3);

		//std::cout << "u1u2u3BD  " << std::endl;
		//for (int i = 0; i < u1u2u3BD.nx * u1u2u3BD.nx + 2 * u1u2u3BD.nx; i++) {
		//	std::cout << i << " " << u1u2u3BD.u[i] << std::endl;
		//}

		fromDBuTo5Du(&u1u2u3BD, &u1u2u35D);

		//std::cout << "u1u2u35D  " << std::endl;
		//for (int i = 0; i < u1u2u35D.nx * u1u2u35D.nx + 2 * u1u2u35D.nx; i++) {
		//	std::cout << i << " " << u1u2u35D.u[i] << std::endl;
		//}

		reduction_u1u2u3 = from_Full_u_To_Reduction_u(&u1u2u35D);

		//std::cout << "reduction_u1u2u3  " << std::endl;
		//for (int i = 0; i < reduction_u1u2u3.n; i++) {
		//	std::cout << i << " " << reduction_u1u2u3.u[i] << std::endl;
		//}

		ReductionMatrixAndVectorF(matr, &reduc_m_test, f5D, &reduc_f_test, &reduction_u1u2u3);

		//std::cout << "reduc_f_test  " << std::endl;
		//for (int i = 0; i < reduc_f_test.nx * reduc_f_test.nx + 2 * reduc_f_test.nx; i++) {
		//	std::cout << i << " " << reduc_f_test.u[i] << std::endl;
		//}

		Init_MyZeroVector(&baby_zero);
		count_iter_5D += CalculateChebaOneStep(&Reduction_cheba_for_D5, &reduc_m_test, &reduc_f_test, eps, &baby_zero);
		
		//std::cout << "baby_zero  " << std::endl;
		//for (int i = 0; i < baby_zero.nx * baby_zero.nx + 2 * baby_zero.nx; i++) {
		//	std::cout << i << " " << baby_zero.u[i] << std::endl;
		//}

		Init_MyZeroVector(&longred);
		longred = ProlongationVector(&baby_zero, &reduction_u1u2u3);
		
		//std::cout << std::endl;
		//for (int i = 0; i < longred.nx * longred.ny + 2 * longred.nx; i++) {
		//	std::cout << i << " " << longred.u[i] << std::endl;
		//}

		Init_MyZeroVector(&all_solve);
		all_solve = Prolongation(&longred, matr, f5D);
		
		//std::cout << std::endl;
		//for (int i = 0; i < all_solve.nx * all_solve.ny + 2 * all_solve.nx; i++) {
		//	std::cout << i << " " << all_solve.u[i] << std::endl;
		//}

		from5DuToBDu(&all_solve, &u1u2u3BD);

		//for (int i = 0; i < u1u2u3BD.nx * u1u2u3BD.ny + 2 * u1u2u3BD.nx; i++) {
		//	std::cout << i << " " << u1u2u3BD.u[i] << std::endl;
		//}

		UnglueVectorU(&u1u2u3BD, &u1, &u2, &u3);

		//std::cout << "u1" << std::endl;
		//for (int i = 0; i < u1.n; i++) {
		//	std::cout << i << " " << u1.u[i] << std::endl;
		//}
		//std::cout << "u2" << std::endl;
		//for (int i = 0; i < u2.n; i++) {
		//	std::cout << i << " " << u2.u[i] << std::endl;
		//}
		//std::cout << "u3" << std::endl;
		//for (int i = 0; i < u3.n; i++) {
		//	std::cout << i << " " << u3.u[i] << std::endl;
		//}

		glueVectorU(u0, &u1, &u2, &u3);
		fromDBuTo5Du(u0, &resultU5D);
		
		//for (int i = 0; i < u0->nx * u0->ny + 2 * u0->nx; i++) {
		//	std::cout << i << " " << resultU5D.u[i] << std::endl;
		//}

		My_Myltiplication_MonV(matr, &resultU5D, &support1);

		//std::cout << "support1" << std::endl;
		//for (int i = 0; i < support1.nx * support1.ny + 2 * support1.nx; i++) {
		//	std::cout << support1.u[i] << std::endl;
		//}

		VectorSub(&support1, f5D, &rn);

		//std::cout << "rn" << std::endl;
		//for (int i = 0; i < support1.nx * support1.ny + 2 * support1.nx; i++) {
		//	std::cout << rn.u[i] << std::endl;
		//}
	}

	//while ((VectorScalar(&rn, &rn) >= eps * VectorScalar(f, f)) && count < MaxIter) {
	//std::cout << "VectorScalar(&rn, &rn) " << VectorScalar(&rn, &rn) << " "<< eps * VectorScalar(f5D, f5D) << "  "<< (VectorScalar(&rn, &rn) >= eps * VectorScalar(f5D, f5D)) << std::endl;
	std::cout << count << " |rn| = " << VectorScalar(&rn, &rn) << std::endl;
	std::cout << "h = " <<nx+1 << std::endl;
	std::cout << "external iterations = " << count << " coarse iterations = " << count_iter_BD  << " prolongation iterations = " << count_iter_5D << std::endl;
	return count;

}