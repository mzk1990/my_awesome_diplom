﻿// diplom.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "matrix_struct.h"
#include "cheb.h"
#include <vector>
#include <ctime>
#include <cmath>   
#define PI 3.1415926535897932384626433832795

std::vector<double> equalscalar(int nx, int ny) {
    std::vector<std::vector<double>> M;
    std::vector<double> V;
    std::vector<double> res;
    M.resize(nx * ny);
    V.resize(nx * ny);
    res.resize(nx * ny);
    for (int i = 0; i < nx * ny; i++) {
        V[i] = 1;
    }
    for (int i = 0; i < nx * ny; i++) {
        res[i] = 0;
    }
    for (int i = 0; i < nx * ny; i++) {
        M[i].resize(nx * ny);
    }

    for (int i = 0; i < nx * ny; i++) {
        for (int j = 0; j < nx * ny; j++) {
            if (i == j) {
                M[i][j] = 4;
            }
            if (i == j - 1) {
                if ((i + 1) % nx == 0) {
                    M[i][j] = 0;
                }
                else {
                    M[i][j] = -1;
                }
            }
            if (i == j + 1) {
                if ((j + 1) % nx == 0) {
                    M[i][j] = 0;
                }
                else {
                    M[i][j] = -1;
                }
            }
            if (i  == j +nx) {
                M[i][j] = -1;
            }
            if (i  == j-nx) {
                M[i][j] = -1;
            }
        }
    }
    for (int i = 0; i < nx * ny; i++) {
        for (int j = 0; j < nx * ny; j++) {
            std::cout << M[i][j] << " ";
        }
        std::cout << std::endl;
    }
    for (int i = 0; i < nx * ny;i++) {
        for (int j = 0; j < nx * ny; j++) {
                res[i] += M[i][j] * V[j];
                std::cout << "res[i] = "<<res[i]<< " M[i][j] = " << M[i][j] << " V[k] = " << V[j] << std::endl;
            }
    }
    return res;
}

int main()
{
    MyDiagMatr2DStruct matr;
    MyDiagMatr2DStruct Reduction_matr;
    MyVectorStruct vect;
    MyVectorStruct f;
    MyVectorStruct init_u;
    MyVectorStruct init_u_test;
    MyVectorStruct cheba_u;
    MyVectorStruct Af;
    MyVectorStruct reduction_u;
    MyVectorStruct vect1;
    MyVectorStruct vect2;
    MyVectorStruct vect3;
    MyVectorStruct vect4;
    MyVectorStruct zeroV;
    MyVectorStruct randV;
    MyVectorStruct randV1;
    MyVectorStruct Reduction_vec;
    MyVectorStruct Reduction_init;
    MyVectorStruct baby_zero;
    MyChebStruct cheba;
    MyChebStruct Reduction_cheba;
    MyChebStructCircly circly;
    MyChebStructCircly circlyOrdered;
    MyChebStructCircly circlyOrderedReduction;
    
    int nx = 31;
    int ny = nx;
    int d = 3;
    //int N = 512;
    int n = nx+1;
    double lmax = 8.0 * sin(n * PI / (2 * n + 2)) *sin(n*PI/(2*n+2));
    double lmin = 8.0 * sin(PI / (2 * n + 2)) * sin(PI / (2 * n + 2));
    double reductionMax = 4.0;
    double reductionMin = 2.0;
    //double lmax = 8.0;
    //double lmin = 0.001;
    double eps = 0.0000001;
    //double eps = 0.0001;
    int iterm = 20000;

    Init_MyDiagMatr2DStruct(nx,ny,d,&matr);
    Init_MyDiagMatr2DStruct((nx+1)/2, (ny+1)/2, d, &Reduction_matr);
    Init_MyVectorStruct(nx, ny, &vect);
    Init_MyVectorStruct(nx, ny, &init_u);
    Init_MyVectorStruct(nx, ny, &init_u_test);
    Init_MyVectorStruct(nx, ny, &cheba_u);
    Init_MyVectorStruct(nx, ny, &f);
    Init_MyVectorStruct(nx, ny, &Af);
    //Init_MyVectorStruct(nx, ny, &vect1);
    //Init_MyVectorStruct(nx, ny, &vect2);
    Init_MyVectorStruct(nx, ny, &vect3);
    Init_MyVectorStruct(nx, ny, &vect4);
    Init_MyVectorStruct(nx, ny, &zeroV);
    Init_MyVectorStruct((nx + 1) / 2, (ny + 1) / 2, &Reduction_vec);
    Init_MyVectorStruct((nx + 1) / 2, (ny + 1) / 2, &Reduction_init);
    Init_MyVectorStruct((nx + 1) / 2, (ny + 1) / 2, &baby_zero);
    //Init_MyVectorStruct(nx, ny, &randV);
    //Init_MyVectorStruct(nx, ny, &randV1);
    MyConstr_Matr_2D5DS_RD(&matr);
    MyConstr_MatrReduction(&Reduction_matr);
    //Init_My123Vector(&init_u);
    Init_MyEyeVector(&init_u);
    Init_MyZeroVector(&init_u_test);
    //Init_sinxsiny(&init_u);
    //Init_MyEyeVector(&vect1);
    //Init_MyTooEyeVector(&vect2);
    Init_MyZeroVector(&cheba_u);
    Init_MyZeroVector(&vect3);
    Init_MyZeroVector(&zeroV);
    Init_MyEyeVector(&vect4);
    Init_MyZeroVector(&f);
    Init_MyZeroVector(&Af);
    Init_MyReductionVector(&Reduction_vec);
    Init_MyEyeVector(&baby_zero);
    Init_MyEyeVector(&Reduction_init);
    //Init_MyRandomVector(&init_u);
    //Init_MyRandomVector(&randV1);
    //VectorSub(&vect1, &vect2, &vect3);
    //Init_ChebStructCircly(&circly, lmax, lmin, iterm, N);
    //Init_ChebStructCircly(&circlyOrdered, lmax, lmin, iterm, N);
    //Init_ChebStructCircly(&circlyOrderedReduction, reductionMax, reductionMin, iterm, N);
    //ConstrChebCirclyNotOrdered(&circly);
    //ConstrChebCircly(&circlyOrdered);
    //ConstrChebCircly(&circlyOrderedReduction);
    Init_ChebStruct(&cheba, lmax, lmin, iterm);
    Init_ChebStruct(&Reduction_cheba, reductionMax, reductionMin, iterm);
    ConstrCheb(&cheba);
    ConstrCheb(&Reduction_cheba);
    int i = 0;
    //matr.a[16] = 1000;
    //for (; i < d * nx * ny + 3 * nx - 2; i++) {
    //    std::cout << Reduction_matr.a[i]  <<std::endl;
    //}
    //std::cout << i << " "<<std::endl;
    //My_Myltiplication_MonV(&Reduction_matr, &randV1, &vect4);
    //My_Myltiplication_MonV(&matr, &init_u, &f);
    //for ( i = 0; i < nx * ny + 2 * nx; i++) {
    //    std::cout << vect4.u[i] << " ";
    //}
    //std::cout <<" " << std::endl;
    bool flag = true;
    //for (i = 0; i < nx * ny + 2 * nx; i++) {
    //    std::cout << Reduction_vec.u[i] <<" "<<vect4.u[i]<< std::endl;
    ////    std::cout << Reduction.u[i] << " i = " << i << std::endl;
    //    if (!(fabs(Reduction_vec.u[i] - vect4.u[i]) < std::numeric_limits<double>::epsilon())) {
    //        flag = false;
    //        break;
    //    }
    //}
    //for (int i = 0; i < nx * ny + 2 * nx; i++) {
    //    std::cout << randV.u[i] << " i = " << i << std::endl;
    //}
    //int t = CalculateChebaRichards(&cheba, &matr, &vect, eps, &zeroV);
    //int t = CalculateCheba(&cheba, &matr, &vect, eps, &zeroV);
    //int t = CalculateChebaOneStep(&Reduction_cheba, &Reduction_matr, &Reduction_vec, eps, &randV);
    //int t = CalculateChebaOneStep(&Reduction_cheba, &Reduction_matr, &vect4, eps, &randV);
    //int t = CalculateChebaCirclyNotOrdered(&circlyOrderedReduction, &Reduction_matr, &vect4, eps, &randV);
    My_Myltiplication_MonV(&matr, &init_u, &f);
    unsigned int start_time = clock();
    //int t = CalculateChebaOneStep(&cheba, &matr, &f, eps, &cheba_u);
    unsigned int end_time = clock(); // конечное время
    //int t = CalculateHalfStatic(&cheba, &matr, &vect, eps, &zeroV);

    //int t = CalculateChebaCirclyNotOrdered(&circly, &matr, &vect, eps, &zeroV);
    //int t = CalculateChebaCirclyNotOrdered(&circlyOrdered, &matr, &vect, eps, &zeroV);
    std::cout.precision(16);
    
  
    My_Myltiplication_MonV(&matr, &cheba_u, &Af);

  //My_Myltiplication_MonV(&Reduction_matr, &randV, &vect3);
  //std::cout << "  iter =    " << t << std::endl;
  std::cout << "  vector f    " <<  "vector u"<< "  vector A*u" << "         randV1" << std::endl;
  for (int i = 0; i < nx * ny + 2 * nx; i++) {
  //std::cout << i << " " << f.u[i]            <<std::endl;
  }
  //std::cout << "  iter =    " << t << std::endl;
  std::cout << "  flag =    " << flag << std::endl;


    //int t = CalculateChebaOneStep(&cheba, &matr, &vect4, eps, &zeroV);

    //std::cout << "  iter =    " << t << std::endl;
    std::cout << "  vector f    " << "vector u" << "                                    vector A*u" << std::endl;
    for (int i = 0; i < nx * ny + 2 * nx; i++) {
            //std::cout << i << " "              << f.u[i] << "           " << cheba_u.u[i] << "            " << Af.u[i] << std::endl;
     }
    //std::cout << "  iter =    " << t << std::endl;
    std::cout << "  time =    " << (end_time - start_time)/ CLK_TCK << std::endl;
    //reduction_u = fromFulluToReductionu(&cheba_u);
    ////for (int i = 0; i < reduction_u.nx * reduction_u.ny + 2 * reduction_u.nx; i++) {
    ////    std::cout <<i<< " "<< reduction_u.u[i] << std::endl;
    ////}
    //std::cout << std::endl;
    //
    //MyVectorStruct full_u = fromReductionuToFullu(&reduction_u,&f,&matr);
    //std::cout << "  full_u    " << "         cheba_u" << std::endl;
    //for (int i = 0; i < full_u.nx * full_u.ny + 2 * full_u.nx; i++) {
    //    if (!(abs(full_u.u[i] - init_u.u[i]) <= 0.1)) {
    //        std::cout << i << " " << full_u.u[i] << " " << cheba_u.u[i] << "  " << init_u.u[i] << "  " << (abs(full_u.u[i] - init_u.u[i]) <= 0.1) << std::endl;
    //    }
    //}
    std::cout << std::endl;
    
    //My_Myltiplication_MonV(&matr, &init_u, &f);

    justVector reduction_u1 = from_Full_u_To_Reduction_u(&cheba_u);
    std::cout << "reduction_u1 " << std::endl;
    for (int i = 0; i < reduction_u1.n; i++) {
       
        //std::cout << i << " " << reduction_u1.u[i] <<  std::endl;

    }



    std::cout << std::endl;
    for (int i = 0; i < init_u.nx*init_u.ny + 2* init_u.nx; i++) {
        //std::cout << i << " "             << f.u[i] << std::endl;

    }

    MyDiagMatr2DStruct reduc_m;
    MyVectorStruct reduc_f;
    ReductionMatrixAndVectorF(&matr, &reduc_m, &f, &reduc_f, &reduction_u1);
    std::cout << "reduc_f " << std::endl;
    for (int i = 0; i < reduc_f.nx * reduc_f.ny + 2 * reduc_f.nx; i++) {
        
        //std::cout << i << " " << reduc_f.u[i] << std::endl;

    }

    justVector reduction_u2 = from_Full_u_To_Reduction_u2(&f);
    //std::cout << "reduction_u2 " << std::endl;
    for (int i = 0; i < reduction_u2.n; i++) {

        //std::cout << i << " " << reduction_u2.u[i]<< "   reduc_f " << reduc_f.u[i+reduc_f.nx] << std::endl;

    }

    
    //int reduction_iter = CalculateChebaOneStep(&Reduction_cheba, &reduc_m, &reduc_f, eps, &baby_zero);
    std::cout << std::endl;
    std::cout << "baby_zero " << std::endl;
    for (int i = 0; i < baby_zero.nx * baby_zero.ny + 2 * baby_zero.nx ; i++) {
        //std::cout << i << " " << baby_zero.u[i] << std::endl;

    }
    
    //std::cout << std::endl;
    //for (int i = 0; i < Reduction_vec.nx * Reduction_vec.ny + 2 * Reduction_vec.nx; i++) {
    //    std::cout << i << " "  << baby_zero.u[i] << std::endl;

    //}

    //MyVectorStruct longred = ProlongationVector(&baby_zero, &reduction_u1); 
    //std::cout << std::endl;
    //for (int i = 0; i < longred.nx * longred.ny + 2 * longred.nx; i++) {
    //    //std::cout << i << " " << longred.u[i] << std::endl;

    //}

    //MyVectorStruct longred23 = Prolongation(&longred, &matr,&f);
    //std::cout << std::endl;
    //for (int i = 0; i < longred23.nx * longred23.ny + 2 * longred23.nx; i++) {
    //    //std::cout << i << " " << longred23.u[i] <<"  "<< cheba_u.u[i]<< std::endl;

    //}
    //MyVectorStruct zero_u;
    //Init_MyVectorStruct(nx, ny, &zero_u);
    //Init_MyZeroVector(&zero_u);
    //
    //justVector jtest = A31u1(&zero_u);

    //for (int i = 0; i < jtest.n; i++) {
    //    std::cout << i << " " << jtest.u[i] <<" " <<init_u.u[init_u.nx+i]<< std::endl;

    //}
    //std::cout << std::endl;
    //justVector jD31u3 = Tetta1D31u3(&init_u, 1);

    //for (int i = 0; i < jD31u3.n; i++) {
    //    std::cout << i << " " << jD31u3.u[i] << " " << init_u.u[nx + (nx * nx - 1) / 2 + 1 + i] << std::endl;

    //}
    //std::cout << std::endl;
    //justVector f31 = f3(&f,&jD31u3,&jtest);

    //for (int i = 0; i < f31.n; i++) {
    //    std::cout << i << " " << f31.u[i] << std::endl;

    //}
    //std::cout << std::endl;
    //justVector A23f31 = A23f3(&f31);

    //for (int i = 0; i < A23f31.n; i++) {
    //    std::cout << i << " " << A23f31.u[i] << std::endl;

    //}
    //std::cout <<"new_f"<< std::endl;
    //MyVectorStruct newf2 = newF2(&f, &A23f31);

    //for (int i = 0; i < newf2.nx* newf2.ny + 2* newf2.nx; i++) {
    //    std::cout << i << " " << newf2.u[i] << std::endl;

    //}
    //std::cout << std::endl;
    //for (int i = 0; i < f.nx * f.ny + 2 * f.nx; i++) {
    //    //std::cout << i << " " << f.u[i] << std::endl;

    //}
    //std::cout << std::endl;
    //MyDiagMatr2DStruct redtestM;
    //Init_MyDiagMatr2DStruct((nx - 1) / 2, (nx - 1) / 2, d, &redtestM);
    //MyConstr_MatrReductionForCircle(&redtestM);
    //for (int i = 0; i < redtestM.nx * redtestM.ny*d + 3 * redtestM.nx - 2; i++) {
    //    std::cout << i << " " << redtestM.a[i] << std::endl;

    //}

    //std::cout << std::endl;
    //MyChebStruct Reduction_cheba_test;
    //Init_ChebStruct(&Reduction_cheba_test,11./3, 8./3, iterm);
    //ConstrCheb(&Reduction_cheba_test);

    //MyVectorStruct testresult_u;
    //MyVectorStruct result_u;
    //Init_MyVectorStruct(newf2.nx, newf2.ny, &testresult_u);
    //Init_MyVectorStruct(newf2.nx, newf2.ny, &result_u);
    //Init_MyZeroVector(&testresult_u);
    //Init_MyZeroVector(&result_u);
    //int g = CalculateChebaOneStep(&Reduction_cheba_test, &redtestM, &newf2, eps, &testresult_u);
    //std::cout <<g<< std::endl;
    //for (int i = 0; i < testresult_u.nx * testresult_u.ny + 2 * testresult_u.nx; i++) {
    //    std::cout << i << " " << testresult_u.u[i] << std::endl;

    //}

    //MyVectorStruct test1;
    //MyVectorStruct test2;
    //MyVectorStruct test_res;
    //MyVectorStruct u1u2u3BD;
    //MyVectorStruct u1u2u35D;
    //justVector u1;
    //justVector u2;
    //justVector u3;
    //Init_MyVectorStruct(nx, ny, &test1);
    //Init_MyVectorStruct(nx, ny, &test2);
    //Init_MyVectorStruct(nx, ny, &u1u2u3BD);
    //Init_MyVectorStruct(nx, ny, &u1u2u35D);
    //Init_MyVectorStruct((nx - 1)/2, (nx - 1) / 2, &test_res);
    //Init_justVector((nx+1)*(nx+1)/4, &u1);  // точки 
    //Init_justVector((nx - 1) * (nx - 1) / 4, &u2); // кружки - редкое решение
    //Init_justVector(nx*nx - (nx - 1) * (nx - 1) / 4 - (nx + 1) * (nx + 1) / 4, &u3); // крестики
    //ZeroJustVector(&u1);
    //ZeroJustVector(&u2);
    //ZeroJustVector(&u3);
    //Init_My123Vector(&test1);
    //Init_MyZeroVector(&test2);
    //Init_MyZeroVector(&u1u2u3BD);
    //Init_MyZeroVector(&u1u2u35D);
    //Init_MyZeroVector(&test_res);
    ////Init_MyEyeVector(&test1);
    //MyDiagMatr2DStruct Reduction_matr_test;
    //Init_MyZeroVector(&test2);
    //std::cout << std::endl;
    //std::cout << "Reduction_matr_test112313" << std::endl;
    //Init_MyDiagMatr2DStruct((nx-1)/2, (nx - 1) / 2, d, &Reduction_matr_test);
    //for (int i = 0; i < Reduction_matr_test.nx * Reduction_matr_test.ny * d + 3 * Reduction_matr_test.nx - 2; i++) {
    //    std::cout << i << " " << Reduction_matr_test.a[i] << std::endl;
    //}
    //MyConstr_MatrReductionForCircle(&Reduction_matr_test);
    //std::cout << "Reduction_matr_test" << std::endl;
    //for (int i = 0; i < Reduction_matr_test.nx * Reduction_matr_test.ny*d + 3 * Reduction_matr_test.nx - 2; i++) {
    //    std::cout <<i<<" "<< Reduction_matr_test.a[i] << std::endl;
    //}
    //
    //My_Myltiplication_MonV(&matr,&test1,&f);
    ////fromDBuTo5Du(&test1, &test2);
    //VectorEqual(&test1, &f);
    //from5DuToBDu(&f, &test2);
    ////My_Myltiplication_MonV(&redtestM, &testresult_u, &result_u);
    //justVector fBD = from5DfToBDfANDdiv16(&test2);
    //std::cout << "fBD"<< std::endl;
    //for (int i = 0; i < fBD.n; i++) {
    //    std::cout << i<< " " << fBD.u[i] <<  std::endl;
    //}
    //justVector A23fBD = A23f3(&fBD); //!!!!!!!!!!!!!!!!!! тут не верный знак для тестов!!!!!!!!!!!!!

    //std::cout << "A23fBD" << std::endl;
    //for (int i = 0; i < A23fBD.n; i++) {
    //    std::cout << i << " " << A23fBD.u[i] << std::endl;
    //}
    ////MyChebStruct Reduction_cheba_test;
    //Init_ChebStruct(&Reduction_cheba_test,11./3, 8./3, iterm);
    //ConstrCheb(&Reduction_cheba_test);

    //int test_iter = CalculateChebaOneStep(&Reduction_cheba_test, &Reduction_matr_test, &FromJusttoVector(&A23fBD), eps, &test_res);

    //std::cout << "test_iter  " << test_iter <<std::endl;
    //for (int i = 0; i < test_res.nx* test_res.nx + 2* test_res.nx; i++) {
    //    std::cout << i << " " << test_res.u[i] << std::endl;
    //}
    //updateFromVectorToJust(&test_res, &u2);
    //std::cout << "u2" << std::endl;
    //for (int i = 0; i < u2.n; i++) {
    //    std::cout << i << " " << u2.u[i] << std::endl;
    //}

    //glueVectorU(&u1u2u3BD, &u1, &u2, &u3);
    //std::cout << "u1u2u3BD  " << std::endl;
    //for (int i = 0; i < u1u2u3BD.nx * u1u2u3BD.nx + 2 * u1u2u3BD.nx; i++) {
    //    std::cout << i << " " << u1u2u3BD.u[i] << std::endl;
    //}

    //fromDBuTo5Du(&u1u2u3BD, &u1u2u35D);

    //std::cout << "u1u2u35D  " << std::endl;
    //for (int i = 0; i < u1u2u35D.nx * u1u2u35D.nx + 2 * u1u2u35D.nx; i++) {
    //    std::cout << i << " " << u1u2u35D.u[i] << std::endl;
    //}

    //justVector reduction_u1u2u3 = from_Full_u_To_Reduction_u(&u1u2u35D);
    //std::cout << "reduction_u1u2u3  " << std::endl;
    //for (int i = 0; i < reduction_u1u2u3.n; i++) {
    //    std::cout << i << " " << reduction_u1u2u3.u[i] << std::endl;
    //}

    //MyDiagMatr2DStruct reduc_m_test;
    //MyVectorStruct reduc_f_test;
    //ReductionMatrixAndVectorF(&matr, &reduc_m_test, &f, &reduc_f_test, &reduction_u1u2u3);

    //std::cout << "reduc_f_test  " << std::endl;
    //for (int i = 0; i < reduc_f_test.nx * reduc_f_test.nx + 2 * reduc_f_test.nx; i++) {
    //    std::cout << i << " " << reduc_f_test.u[i] << std::endl;
    //}
    //Init_MyZeroVector(&baby_zero);
    //int reduction_iter_test = CalculateChebaOneStep(&Reduction_cheba, &reduc_m_test, &reduc_f_test, eps, &baby_zero);
    //std::cout << "baby_zero  " << std::endl;
    //for (int i = 0; i < baby_zero.nx * baby_zero.nx + 2 * baby_zero.nx; i++) {
    //    std::cout << i << " " << baby_zero.u[i] << std::endl;
    //}
    //Init_MyZeroVector(&longred);
    //longred = ProlongationVector(&baby_zero, &reduction_u1u2u3);
    //std::cout << std::endl;
    //for (int i = 0; i < longred.nx * longred.ny + 2 * longred.nx; i++) {
    //    std::cout << i << " " << longred.u[i] << std::endl;

    //}
    //Init_MyZeroVector(&longred23);
    //longred23 = Prolongation(&longred, &matr, &f);
    //std::cout << std::endl;
    //for (int i = 0; i < longred23.nx * longred23.ny + 2 * longred23.nx; i++) {
    //    std::cout << i << " " << longred23.u[i] << std::endl;
    //}

    //from5DuToBDu(&longred23, &u1u2u3BD);
    //for (int i = 0; i < u1u2u3BD.nx * u1u2u3BD.ny + 2 * u1u2u3BD.nx; i++) {
    //        std::cout << i << " " << u1u2u3BD.u[i] << std::endl;
    //}

    //UnglueVectorU(&u1u2u3BD, &u1, &u2, &u3);

    //std::cout << "u1" << std::endl;
    //for (int i = 0; i < u1.n; i++) {
    //    std::cout << i << " " << u1.u[i] << std::endl;
    //}
    //std::cout << "u2" << std::endl;
    //for (int i = 0; i < u2.n; i++) {
    //    std::cout << i << " " << u2.u[i] << std::endl;
    //}
    //std::cout << "u3" << std::endl;
    //for (int i = 0; i < u3.n; i++) {
    //    std::cout << i << " " << u3.u[i] << std::endl;
    //}
    //std::cout << "u1" << std::endl;
    //for (int i = 0; i < u1.n; i++) {
    //    std::cout << i << " " << u1.u[i] << std::endl;
    //}
    //justVector A31u1;
    //Init_justVector((nx * nx - 1) / 2, &A31u1);
    //A31u1just(&u1, &matr, &A31u1);

    //std::cout << "A31u1" << std::endl;
    //for (int i = 0; i < A31u1.n; i++) {
    //    std::cout << i << " " << A31u1.u[i] << std::endl;
    //}

    //justVector Tetta1D31u3;
    //Init_justVector((nx * nx - 1) / 2, &Tetta1D31u3);
    //Tetta1D31u3just(&u3, &matr, &Tetta1D31u3, 1.);
    //std::cout << "Tetta1D31u3" << std::endl;
    //for (int i = 0; i < Tetta1D31u3.n; i++) {
    //    std::cout << i << " " << Tetta1D31u3.u[i] << std::endl;
    //}

    MyVectorStruct test1;
    double tetta = 1;
    Init_MyVectorStruct(nx, ny, &test1);
    Init_MyEyeVector(&test1);
    My_Myltiplication_MonV(&matr, &test1, &f);
    MyVectorStruct u01;
    Init_MyVectorStruct(nx, ny, &u01);
    Init_MyZeroVector(&u01);
    int co = find_u1u2u3(&matr,&u01,&f, iterm, eps, tetta);
    std::cout << "co = " <<co << std::endl;
    MyVectorStruct u015D;
    Init_MyVectorStruct(nx, ny, &u015D);
    Init_MyZeroVector(&u015D);
    fromDBuTo5Du(&u01,&u015D);
    std::cout << "u01" << std::endl;
    //for (int i = 0; i < u015D.nx* u015D.nx + 2* u015D.nx; i++) {
    //    std::cout << i << " " << u015D.u[i] << std::endl;
    //}
    //MyDiagMatr2DStruct reduc_matrix_test;
    //MyVectorStruct reduc_f_test;
    //ReductionMatrixAndVectorF(&matr, &reduc_matrix_test, &f, &reduc_f_test, &FromVectortoJust(&testresult_u));



    //CalculateChebaOneStep(&Reduction_cheba, &reduc_matrix_test, &reduc_f_test,eps,&baby_zero);

    //std::cout << std::endl;
    //std::cout << "baby_zero " << std::endl;
    //for (int i = 0; i < baby_zero.nx * baby_zero.ny + 2 * baby_zero.nx; i++) {
    //    std::cout << i << " " << baby_zero.u[i] << std::endl;

    //}
    //MyVectorStruct testresult_u_longred = ProlongationVector(&baby_zero, &FromVectortoJust(&testresult_u));
    //std::cout << std::endl;
    //for (int i = 0; i < reduc_f_test.nx * reduc_f_test.ny + 2 * reduc_f_test.nx; i++) {
    //    std::cout << i << " " << reduc_f_test.u[i] << std::endl;

    //}
    //MyVectorStruct longred2343 = Prolongation(&testresult_u_longred, &matr, &f);
    //std::cout << std::endl;
    //for (int i = 0; i < testresult_u_longred.nx * testresult_u_longred.ny + 2 * testresult_u_longred.nx; i++) {
    //    std::cout << i << " " << longred2343.u[i] << std::endl;
    //}

    //MyVectorStruct calculateReduse_U = find_u2(&init_u, &matr);

    //std::cout << "IIIIIIIIIIIII"<<std::endl;
    //for (int i = 0; i < calculateReduse_U.nx * calculateReduse_U.ny + 2 * calculateReduse_U.nx; i++) {
    //    std::cout << i << " " << calculateReduse_U.u[i] << std::endl;
    //}
    //std::cout.precision(3);
    //double absolute_error_L_U = VectorError(&longred23, &init_u);
    //double absolute_error_C_U = VectorError(&cheba_u, &init_u);
    //double relative_error_L_U = absolute_error_L_U/ VectorScalar(&init_u, &init_u);
    //double relative_error_C_U = absolute_error_C_U / VectorScalar(&init_u, &init_u);
    //std::cout << "VectorScalar(&init_u, &init_u) = " << VectorScalar(&init_u, &init_u) << std::endl;
    //std::cout << "h = " << nx + 1 << std::endl;
    //std::cout << "iter = " << reduction_iter << std::endl;
    //std::cout <<"absolute_error_L_U = "<< absolute_error_L_U <<std::endl
    //    <<"absolute_error_C_U = " << absolute_error_C_U << std::endl <<"relative_error_L_U = "<< relative_error_L_U << std::endl <<"relative_error_C_U = " << relative_error_C_U << std::endl <<std::endl;
    //
    //std::cout <<  absolute_error_L_U << " " << absolute_error_C_U << " " << relative_error_L_U << " " << relative_error_C_U << " " << std::endl;
    //std::cout << VectorError(&vect4, &f) << std::endl;    
    //std::cout <<"n(esp) <= "<< 0.5*abs(log(eps/2))*sqrt(reductionMax / reductionMin) + 1 << std::endl;
    //std::cout << "  iter =    " << t << std::endl;
    //std::cout << "  " << t << std::endl;

    //std::cout << nx + 1<< std::endl;
    //std::cout << reduction_iter << std::endl;
    //std::cout << absolute_error_L_U << std::endl;
    //std::cout << t << std::endl;
    //std::cout << absolute_error_C_U << std::endl;


    //    int i = 0;
    //    double g = (lmax + lmin) / (lmax - lmin);
    //    std::cout << "g0 = " << 0 << std::endl;
    //    std::cout << "g1 = " << 1/(2*g*g-1) << std::endl;
    //    std::cout << "g2 = " << 1/(4*g*g-3) << std::endl;
    //    std::cout << "g3 = " << (2*g*g -1) / (8*g*g*g*g - 8*g*g +1) << std::endl;
    //    std::cout << "g4 = " << (4 * g * g * g - 3) / (16 * g * g * g * g * g - 20 * g * g * g + 5 * g) << std::endl;
    //    while (cheba.gamma_n[i] != cheba.gamma_n[i+1]) {
    //        std::cout << "i = " << i << std::endl;
    //        std::cout << "a[i] = " << cheba.alpha_n[i] << std::endl;
    //        std::cout << "b[i] = " << cheba.beta_n[i] << std::endl;
    //        std::cout << "g[i] = " << cheba.gamma_n[i] << std::endl;
    //        i++;
    //    }
    //    std::cout << "a[i] = " << cheba.alpha_n[i] << std::endl;
    //    std::cout << "b[i] = " << cheba.beta_n[i] << std::endl;
    //    std::cout << "g[i] = " << cheba.gamma_n[i] << std::endl;
    //    std::cout << "  iter =    " << t << std::endl;
        //int t = CalculateChebaCirclyNotOrdered(&circly, &matr, &vect, eps, &zeroV);
        //std::cout << "  iter =    " << t << std::endl;
        //My_Myltiplication_MonV(&matr, &zeroV, &vect4);
        //std::cout << "  vector f    " << "vector u" << "  vector A*u" << std::endl;
        //for (int i = 0; i < nx * ny + 2 * nx; i++) {
        //    std::cout << i << " " << vect.u[i] << "           " << zeroV.u[i] << "            " << vect4.u[i] << std::endl;
        //}
        //std::cout << std::endl;
        //for (int i = 1; i <= N; i++) {
        //    std::cout << "i= "<<i<<" "<<2.0 / (lmax + lmin - (lmax - lmin) * cos((2 * i - 1) * PI / (2 * N))) << std::endl;
        //}
        //std::cout << std::endl;
        //for (int i = 1; i <= circly.N; i++) {
        //    std::cout << circly.T_n[i] << std::endl;
        //}
    //double c = 0;
    //for (int i = 1; i <= 8; i++) {
    //    c = 2.0 / (lmax + lmin - (lmax - lmin) * abs(cos((2 * i - 1) * PI / 16)));
    //    std::cout << i << " " << c <<" " << cos((2*i - 1)*PI/16) <<std::endl;
    //}


    //Init_ChebStruct(&cheba, lmax, lmin, iterm);
    //ConstrCheb(&cheba);
    //int N = 8;
    //auto Tn = giveParam(N);
    //auto index = (double*)malloc(( N + 1 ) * sizeof(double));
    //for (int i = 1; i < N + 1; i++) {
    //    std::cout<<"i = "<<i<<"  "<< Tn[i]<<std::endl;
    //}

    //for (int n = 1; n <= N; n++) {
    //    std::cout<<"n = "<<n<<"  "<< 2.0 / (lmax + lmin - (lmax - lmin) * cos((2 * n - 1) * PI / (2 * 2*N)))<<std::endl;

    //    index[n] = 2.0 / (lmax + lmin - (lmax - lmin) * cos(((2 * (double)Tn[n]-1)) * PI / (2 * 2 * N)));
    //}

    //        for (int n = 1; n <= N; n++) {
    //    std::cout<<"n = "<<n<<"  "<< 2.0 / (lmax + lmin - (lmax - lmin) * cos((2 * n - 1) * PI / (2 * 2*N)))<<std::endl;
    //    std::cout << "n = " << n << "  " << cos((2 * n - 1) * PI / (2 * 2*N)) << std::endl;
    //    index[n] = 2.0 / (lmax + lmin - (lmax - lmin) * cos(((2 * (double)Tn[n]-1)) * PI / (2 * 2 * N)));
    //}

    //for (int i = 1; i < N+1; i++) {
    //    std::cout << "i = " << i << "  " << index[i] << std::endl;
    //}
//    int t = CalculateCheba(&cheba, &matr, &vect, eps, &zeroV);
//    for (int i = 0; i < iterm; i++) {
//    std::cout << i <<"   "<<cheba.gamma_n[i] << std::endl;
//}
//std::cout <<"   " << std::endl;
//
//for (int i = 0; i < iterm; i++) {
//    std::cout << i << "   " << cheba.alpha_n[i] << std::endl;
//}
//std::cout << "   " << std::endl;
//
//for (int i = 0; i < iterm; i++) {
//    std::cout << i << "   " << cheba.beta_n[i] << std::endl;
//}
//std::cout << "   " << std::endl;
    //
    //My_Myltiplication_MonV(&matr, &zeroV, &vect4);
    //std::cout << "  vector f    " <<  "vector u"<< "  vector A*u" << std::endl;
    //for (int i = 0; i < nx * ny + 2 * nx; i++) {
    //    std::cout << i << " " << vect.u[i] << "           "<< zeroV.u[i] <<"            " << vect4.u[i] << std::endl;
    //}

    //std::cout << t << " = iter" << std::endl;
//    std::cout << "count iter" << std::endl;
//    std::cout << t << std::endl;
//    std::cout << "vector u" << std::endl;
//for (int i = 0; i < nx*ny+2*nx; i++) {
//    std::cout << i << "   " << zeroV.u[i] 
//        << std::endl;
//}
//My_Myltiplication_MonV(&matr, &zeroV, &vect4);
//std::cout << "vector A*u" << std::endl;
//for (int i = 0; i < nx * ny + 2 * nx; i++) {
//    std::cout << i << "   " << vect4.u[i] << std::endl;
//}
//std::cout << "matrix A" << std::endl;
//for (int i = 0; i < d * nx * ny + (3 * nx - 2); i++) {
//    std::cout << i << "   " << matr.a[i] << std::endl;
//}





























































    //for (int i = 0; i < nx*ny*d + 3*nx - 2; ++i) {
    //    std::cout << i <<"   "<<matr.a[i] << std::endl;
    //}
    //std::cout <<"   " << std::endl;


    //for (int i = 0; i < nx * ny +2*nx; ++i) {
    //    std::cout << i << "   " << vect.u[i] << std::endl;
    //}
    //std::cout << "   " << std::endl;

    //for (int i = 0; i < nx * ny + 2 * nx; ++i) {
    //    std::cout << i << "   " << zeroV.u[i] << std::endl;
    //}
    //std::cout << "   " << std::endl;

    //My_Myltiplication_MonV(&matr, &vect, &zeroV);

    //for (int i = 0; i < nx * ny + 2 * nx; ++i) {
    //    std::cout << i << "   " << zeroV.u[i] << std::endl;
    //}
    //std::cout << "   " << std::endl;
    //auto res = equalscalar(nx, ny);
    //
    //for (int i = 0; i < nx * ny; ++i) {
    //    std::cout << i << "   " << res[i] << std::endl;
    //}

    //for (int i = 0; i < nx * ny; i++) {
    //    if (res[i] == zeroV.u[i + nx]) {
    //        //continue;
    //        std::cout << "good" << std::endl;
    //        continue;
    //    }

    //        std::cout << "bad" << std::endl;

    //}

}

